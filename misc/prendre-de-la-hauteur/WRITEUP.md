Writeup
=======

Comme le suggère le nom du challenge, il suffit de prendre de la hauteur afin d'identifier le flag.

Exploitation&nbsp;:

 - Ouvrir le fichier dans un éditeur de texte&nbsp;;
 - Dézoomer au maximum les données.

Flag&nbsp;: `ENSIBS{T4k3_1t_t0_th3_t0p}`
