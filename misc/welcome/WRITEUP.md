Writeup
=======

Un peu de politesse ne fait pas de mal. Il suffit de répondre `bonjour`.

Flag&nbsp;: `ENSIBS{W3lc0M3_b0nJ0ur!}`
