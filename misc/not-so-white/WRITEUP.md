Writeup
=======

## LVL1

César dont l'offset est calculé à partir de la première lettre de la saisie utilisateur (p. ex., le `a` est le permier caractère de l'alphabet et ne provoque donc aucun décalage).

Exploitation&nbsp;:

```bash
pushd exploit/
python lvl1.py
popd
```

Flag&nbsp;: `ENSIBS{PyTh0n_4_3ver}`

## LVL2

Décalage avec des puissances de 2 modulo la longeur de l'alphabet (une analyse de la position du caractère obtenu par rapport à la position du caractère d'origine permet d'observer les puissances de 2).

Exploitation&nbsp;:

```bash
pushd exploit/
python lvl2.py
popd
```

Flag&nbsp;: `ENSIBS{PyTh0n_w1ll_n3ver_d13}`

## LVL3

Idem que not so white 2, mais la saisie de l'utilisateur est d'abord inversée ('abcd' => 'cdba'), ça ajoute un peu de brainfuck.

Exploitation&nbsp;:

```bash
pushd exploit/
python lvl3.py
popd
```

Flag&nbsp;: `ENSIBS{7ru57_m3_1m_PyTh0n_guy}`

## LVL4

Chaque lettre est décalée dans l'alphabet en se basant sur un offset correspondant à la somme des positions respectives dans l'alphabet des caractères suivants dans la saisie utilisateur.

Exploitation&nbsp;:

```bash
pushd exploit/
python lvl4.py
popd
```

Flag&nbsp;: `ENSIBS{53cur17y_8y_0b5cur17y}`

## LVL5

XOR.

Exploitation&nbsp;:

```bash
pushd exploit/
python lvl5.py
popd
```

Flag&nbsp;: `ENSIBS{n3v3r_g0nN4_g1v3_uP_PyTh0n}`
