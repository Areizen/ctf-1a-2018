# Not so White?

Un développeur peu scrupuleux semble ignorer le principe de sécurité par l'obscurité.

Obtenez un accès complet à ses applications pour lui prouver son erreur.

<u>**URI&nbsp;:**</u>

 - LVL1: [tcp://notsowhite.ensibs.ctf:7770](tcp://notsowhite.ensibs.ctf:7770)
 - LVL2: [tcp://notsowhite.ensibs.ctf:7771](tcp://notsowhite.ensibs.ctf:7771)
 - LVL3: [tcp://notsowhite.ensibs.ctf:7772](tcp://notsowhite.ensibs.ctf:7772)
 - LVL4: [tcp://notsowhite.ensibs.ctf:7773](tcp://notsowhite.ensibs.ctf:7773)
 - LVL5: [tcp://notsowhite.ensibs.ctf:7774](tcp://notsowhite.ensibs.ctf:7774)
