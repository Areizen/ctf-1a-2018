#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Author: Baptiste MOINE <contact@bmoine.fr>.

"""Server."""

# Generate documentation: epydoc -v --html main.py -o ./docs

import argparse
import json
from io import open
from random import choice, seed
import socket
import traceback
import threading
from datetime import datetime

from packages import log

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '06 February 2017'

try:
    UNICODE_EXISTS = bool(type(unicode))
except NameError:
    unicode = str

DEBUG = False
DEFAULT_HOST = '0.0.0.0'
DEFAULT_PORT = 8080
DEFAULT_DATA_SIZE = 1024

class Server(object):
    """This class allows to create a server."""
    def __init__(self, host=DEFAULT_HOST, port=DEFAULT_PORT):
        """Constructor of I{Server}.

        @param self: Current instance of I{Server}.
        @param host: Takes '0.0.0.0' as default value (global listening).
        @param port: Takes 8080 as default value.

        @type self: C{Server}
        @type host: C{str} or C{unicode}
        @type port: C{int}
        @raise host: TypeError, host must be a valid (C{str} or C{unicode}).
        @raise host: ValueError, host must be a IP address (x.x.x.x).
        @raise port: TypeError, port must be a valid number (C{int}).
        @raise port: ValueError, port must be a valid TCP port (1024-65535).
        @raise socket: IOError, an error occured while trying to create socket.
        """
        if not isinstance(host, str) and not isinstance(host, unicode):
            raise TypeError('Host must be a valid string!')
        elif not isinstance(port, int):
            raise TypeError('Port must be a valid integer!')
        elif port > 65535 and port < 1024:
            raise ValueError('Port must be a valid TCP port (1024-65535)!')
        else:
            self.port = port
            try:
                socket.inet_aton(host)
            except IOError:
                raise ValueError('Host must be a valid IP address'
                                 ' (e.g., x.x.x.x)!')
            else:
                self.host = host
        try:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.bind((self.host, self.port))
        except IOError:
            raise IOError('An error occured while trying to create socket.')

    def listen(self):
        """Bind socket to TCP port.

        @raise socket: IOError, an error occured while trying to bind socket.
        """
        try:
            self.socket.listen(0)
        except IOError:
            raise IOError('An error occured while trying to bind socket.')
        else:
            log.success('Listening on {host}:{port}.'.format(host=self.host,
                                                             port=self.port))
            log.info('Press CTRL+PAUSE to shutdown the server.')

    def accept(self):
        """Accept connection to socket.

        @raise socket: IOError, an error occured while trying to accept connection to socket.
        """
        try:
            connection, address = self.socket.accept()
        except IOError:
            log.error('An error occured while trying to accept connection to socket.')
        else:
            return (connection, address)

    def close(self):
        """Close socket.

        @raise socket: IOError, an error occured while trying to close socket.
        """
        try:
            self.socket.close()
        except IOError:
            raise IOError('An error occured while trying to close socket.')
        else:
            log.warn('Server has been closed.')
            exit(1)

class ClientHandler(object):
    """This class allows to handle client connection."""
    def __init__(self, flag, connection, address, datasize=DEFAULT_DATA_SIZE):
        """Constructor of I{ClientHandler}.

        @param self: Current instance of I{ClientHandler}.
        @param flag: Flag.
        @param connection: Client connection.
        @param address: Client address.
        @param datasize: Takes 1024 as default value.

        @type self: C{ClientHandler}
        @type flag: C{str} or C{unicode}
        @type connection: C{socket._socketobject}
        @type address: C{tuple}
        @type datasize: C{int}

        @raise datasize: TypeError, port must be a valid number (C{int}).
        """
        self.flag = flag
        self.connection = connection
        self.address = address
        self.datasize = datasize
        self.attempts = 3

    def hello(self):
        """Send a hello message to the client.

        @raise connection: IOError, an error occured while trying to send message to client connection.
        """
        try:
            self.alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789\'",;:!./-+*%~&{}()|$='
            challenge = '''++ Level 2 ++

Max size allowed is {size}.

Char allowed: [{alphabet}]

Find the good password (you've {attempts} attempts)!
'''.format(size=self.datasize, alphabet=self.alphabet, attempts=self.attempts)
            self.connection.send(challenge.encode())
        except IOError:
            log.error('An error occured while trying to send message to client connection.')

    def receive(self, size=DEFAULT_DATA_SIZE):
        """Receive a message of specified size and return received data."""
        if not isinstance(size, int):
            raise TypeError('Size must be a valid integer!')
        elif size <= 0:
            raise ValueError('Size must be greater than zero!')
        else:
            data = self.connection.recv(size + 1)

            return data

    def encode(self, message):
        data = list(message.strip())
        for i in range(len(data)):
            data[i] = self.alphabet[(self.alphabet.find(data[i]) + (2 ** (i + 1))) % len(self.alphabet)]

        return ''.join(data)

    def check(self, message):
        data = self.encode(message)
        expected = self.encode(self.candidate)
        valid = (data == expected)

        return (valid, data, expected)

    def play(self):
        try:
            valid = False
            self.hello()

            while self.attempts > 0 and not valid:
                self.connection.send('\n>> '.encode())
                message = self.receive(self.datasize).decode()
                if not len(message.strip()) == 0:
                    if self.attempts == 3:
                        self.generate()

                    (valid, data, expected) = self.check(message)

                    if valid:
                        self.sendflag()
                    else:
                        status = 'password: {}\nexpected: {}\n'.format(data, expected)
                        self.connection.send(status.encode())
                        self.attempts -= 1
                else:
                    self.connection.send('No no no...\n'.encode())

            self.connection.send('You lose! Try again...\n'.encode())
            self.close()
        except IOError:
            self.close()

    def generate(self):
        candidate = ''
        seed(datetime.now())
        for _ in range(self.datasize):
            candidate += choice(self.alphabet)
        self.candidate = candidate

    def sendflag(self):
        """Send a hello message to the client.

        @raise connection: IOError, an error occured while trying to send message to client connection.
        """
        try:
            message = 'Nice job! {}\n'.format(self.flag)
            self.connection.send(message.encode())
            self.close()
        except IOError:
            log.error('An error occured while trying to send message to client connection.')

    def close(self):
        """Close connection.

        @raise connection: IOError, an error occured while trying to close connection.
        """
        try:
            self.connection.close()
        except IOError:
            raise IOError('An error occured while trying to close connection.')
        else:
            pass
            # log.warn('Connection has been closed.')

def load_config(conf):
    """Load configuration from file."""
    config = dict()

    if conf:
        try:
            with open(conf, mode='rt', encoding='utf-8') as conf_fd:
                config = json.load(conf_fd)
        except IOError:
            log.error('Specified configuration file not found!')
        except ValueError:
            log.error('An error occured while parsing configuration file.')
        else:
            log.success('Loading configurations from {file}.'.format(file=conf))

    return config

def validate_config(conf, needed_keys):
    """Validate and return configuration."""
    type_checked = False

    if conf:
        for key in needed_keys:
            if key['id'] not in conf:
                log.error(('{name} is missing, please check '
                           'your configuration file!').format(name=key['id']))
            elif conf[key['id']] == '':
                log.error(('{name} can\'t be empty, please check '
                           'your configuration file!').format(name=key['name']))
            else:
                for type_ in key['type']:
                    if not type_checked and isinstance(conf[key['id']], type_):
                        type_checked = True
                if not type_checked:
                    log.error('Wrong type for {name} ({type_})!'.format(name=key['name'],
                                                                        type_=type(conf[key['id']]).__name__))
    return conf

def challenge(config):
    """Start challenge."""
    try:
        # Instantiate a new server
        server = Server(config['host'], config['port'])

        # Start the server
        server.listen()

        # Loop for client connection.
        while True:
            # Receive connection from client and send challenge header.
            (connection, address) = server.accept()
            client = ClientHandler(config['flag'], connection, address, config['datasize'])
            threading.Thread(target=client.play).start()
    except (ValueError, TypeError, IOError) as exception_:
        if DEBUG: log.debug(traceback.format_exc())
        log.error(exception_)
    except KeyboardInterrupt:
        try:
            client.close()
        except UnboundLocalError:
            pass
        server.close()
    else:
        try:
            client.close()
        except UnboundLocalError:
            pass
        server.close()

def parse_args():
    """Arguments parsing."""
    parser = argparse.ArgumentParser(
        description='Server v{version}'.format(
            version=__version__
        )
    )

    parser.add_argument('-c', '--config',
                        type=str,
                        default='config.json',
                        help='path to JSON configuration file')

    args = parser.parse_args()

    return args

def main():
    """Receive data from client and store them to database."""
    try:
        # Arguments parsing.
        args = parse_args()

        # Load config from args.config file.
        config = load_config(args.config)

        # Validate global configuration.
        needed_keys = [
            {
                'id': 'flag',
                'name': 'Flag',
                'type': [str, unicode]
            }
        ]

        config = validate_config(config, needed_keys)

        if 'host' not in config:
            config['host'] = DEFAULT_HOST

        if 'port' not in config:
            config['port'] = DEFAULT_PORT

        if 'datasize' not in config:
            config['datasize'] = DEFAULT_DATA_SIZE

        # Start challenge.
        challenge(config)
    except (ValueError, TypeError, OSError) as exception_:
        if DEBUG: log.debug(traceback.format_exc())
        log.error(exception_)

# Runtime processor
if __name__ == '__main__':
    main()
