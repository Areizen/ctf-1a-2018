#!/bin/bash

echo "Stopping Pwn containers"
pushd pwn/crypto-buffer/docker; docker-compose down; popd
pushd pwn/pass-leak/docker; docker-compose down; popd
pushd pwn/rop/docker; docker-compose down; popd

echo "Stopping Misc containers"
pushd misc/welcome/docker; docker-compose down; popd
pushd misc/not-so-white/docker; docker-compose down; popd

echo "Stopping Web containers"
pushd web/electronjs/docker; docker-compose down; popd
pushd web/harpoon/docker; docker-compose down; popd
pushd web/hash/docker; docker-compose down; popd
pushd web/jwatson/docker; docker-compose down; popd
pushd web/myobject/docker; docker-compose down; popd
pushd web/serialkiller/docker; docker-compose down; popd
pushd web/mygame/docker; docker-compose down; popd
pushd web/zoocheck/docker; docker-compose down; popd

echo "Stopping Crypto containers"
pushd crypto/cbc/docker; docker-compose down; popd
pushd crypto/dancingmen/docker; docker-compose down; popd

echo "All containers should be down (please check!)"
screen -ls

