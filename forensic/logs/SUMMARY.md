# Logs

Votre ami possède un blog mais pense s'être fait voler son mot de passe.

Il dispose cependant d'une capture réseau qu'il a pu effectuer pendant le vol.

A vous de retrouver la donnée volée.

<u>**Fichier&nbsp;:**</u> [logs.pcap](files/logs.pcap)
