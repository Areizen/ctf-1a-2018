Writeup
=======

SQLI en Time Based.

Exploitation&nbsp;:

Trier les requètes par ordre chronologique puis regarder les temps de réponse les plus grand.
On récupère les valeurs de comparaison ascii et on les re-traduit en lettre.

```bash
pushd exploit/
pip install -r requirements.txt
python exploit.py
popd
```

Flag&nbsp;: `ENSIBS{SQL1_T1M3_B4S3D_1NJ3CT10N!!}`
