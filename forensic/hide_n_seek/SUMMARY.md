# Hide'n'Seek

Une fuite de donnée a été signalée dans l'un de nos data center, la personne en question semble avoir utilisé divers moyen pour dissimuler les messages et informations exfiltrées.

Par chance, vous avez réussi à intercepter l'une de ses transmissions, il semble s'agir d'un fichier MP3 standard, mais nous savons de source sûre que ce fichier contient des informations secrêtes.

Parviendrez-vous à les retrouver ?

<u>**Fichiers&nbsp;:**</u> [my_favorite_sound.zip](files/my_favorite_sound.zip)
