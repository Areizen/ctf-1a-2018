Writeup
=======

Décodage d'une séquence DTMF cachée dans un fichier MP3 en stéréo.

Exploitation&nbsp;:

 1. Séparation des canaux avec Audacity
 2. Isolation de la bande son DTMF en fin de fichier (suppression du premier canal)
 3. Découpage en fichiers courts (pas plus de 20 secondes)
 4. Décodage des fréquences DTMF avec [dialabc](http://dialabc.com/sound/detect/index.html) ou en Python (`69#78#83#73#66#83#123#51#55#104#49#99#53#95#70#84#87#33#125`)
 5. Décodage en Python, les caractères ASCII sont représentés en décimal, les `#` agissent comme des séparateurs

Flag&nbsp;: `ENSIBS{37h1c5_FTW!}`
