#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

# Generate documentation: epydoc -v --html main.py -o ./docs
# Make tests: python3 -m doctest -v main.py

from packages.time import pretty_time

def encode(input_, sep='#'):
    """Return ASCII-encoded string with sharp as separator.

    >>> encode('test')
    '116#101#115#116'
    >>> encode('Éthique et responsable!')
    '201#116#104#105#113#117#101#32#101#116#32#114#101#115#112#111#110#115#97#98#108#101#33'

    @param input_: Input string.
    @type input_: C{str}
    @raise input_: TypeError, input must be a valid C{str}.

    @param sep: Digits separator.
    @type sep: C{str}
    @raise sep: TypeError, separator must be a valid C{str}.
    @raise sep: ValueError, separator must be a valid separator (from [#*]).
    """
    if not isinstance(input_, str):
        raise TypeError('input must be a valid string!')
    elif not isinstance(sep, str):
        raise TypeError('separator must be a valid string!')
    elif not sep == '*' and not sep == '#':
        raise ValueError('separator must be a valid separator (from [#*])!')
    else:
        output = ''

        i = 1
        for char in input_:
            output += str(ord(char))
            if i < len(input_):
                output += sep
            i += 1

        return output

def decode(input_, sep='#'):
    """Decode ASCII-encoded string with sharp as separator.

    >>> decode('116#101#115#116')
    'test'
    >>> decode('201#116#104#105#113#117#101#32#101#116#32#114#101#115#112#111#110#115#97#98#108#101#33')
    'Éthique et responsable!'

    @param input_: Input string.
    @type input_: C{str}
    @raise input_: TypeError, input must be a valid C{str}.

    @param sep: Digits separator.
    @type sep: C{str}
    @raise sep: TypeError, separator must be a valid C{str}.
    @raise sep: ValueError, separator must be a valid separator (from [#*]).
    """
    if not isinstance(input_, str):
        raise TypeError('input must be a valid string!')
    elif not isinstance(sep, str):
        raise TypeError('separator must be a valid string!')
    elif not sep == '*' and not sep == '#':
        raise ValueError('separator must be a valid separator (from [#*])!')
    else:
        output = ''

        data = input_.split(sep)
        for char in data:
            output += chr(int(char))

        return output

def main():
    """Main process."""
    try:
        print('Decode/Encode ?')
        choice = input('[e/d]> ')
        if choice.lower() == 'd':
            print('Your message:')
            message = input('> ')

            payload = decode(message)
            print('Decoded message: {}'.format(payload))
        else:
            print('Your message:')
            message = input('> ')

            payload = encode(message)
            print(('DTMF Sequence: {payload}\n'
                   'Amplitude: 0.9\n'
                   'Duration: {time}\n'
                   'Duty cycle: 50%').format(payload=payload,
                                             time=pretty_time(len(payload))))
    except (TypeError, ValueError) as exception_:
        print('{!] {}'.format(exception_))

# Runtime processor
if __name__ == '__main__':
    main()
