#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

import urllib
import pyexiv2
from random import choice
import os

DEBUG = True

class Exif:
    def __init__(self, file_):
        self.metadata = pyexiv2.ImageMetadata(file_)
        self.metadata.read()

    def get_attr(self, name):
        return self.metadata[name]

    def set_attr(self, name, value):
        self.metadata[name] = value
        self.metadata.write()

def generate(alphabet, size=6):
    candidate = ''
    for _ in range(size):
        candidate += choice(alphabet)
    return candidate

def hide_secret(secret, directory):
    filelist = []

    for root, dirs, files in os.walk(directory):
        for filename in sorted(files):
            filename = '{directory}/{filename}'.format(directory=directory, filename=filename)
            if filename.endswith('.jpg'):
                if DEBUG: print('{filename}'.format(filename=filename))
                filelist += [filename]

    i = 0
    part_length = 4
    for filename in filelist:
        part = secret[i:i+part_length]
        exif = Exif(filename)
        exif.set_attr('Exif.Image.ImageDescription', part)
        if DEBUG: print('[{i}] in {filename}: {part}'.format(i=i, part=part, filename=filename))
        i += part_length
            
def download_images(directory):
    i = j = 0
    size = 6
    alphabet = 'abcdefghijklmnopqrstuvwxyz0123456789'

    while i < 1000:
        try:
            url = 'https://picsum.photos/720/480?image={num:03d}'.format(num=j)
            filename = '{directory}/{name}.jpg'.format(directory=directory, name=generate(alphabet, size))
            urllib.urlretrieve(url, filename)
        except OSError:
            j += 1
            continue
        else:
            i += 1
            j += 1

def main():
    directory = 'pics'
    secret = 'iVBORw0KGgoAAAANSUhEUgAABIgAAASIAQAAAABQV8luAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAd2KE6QAAAAJcEhZcwAAAFoAAABaAHAjuH0AAAAHdElNRQfiAwYOOR2Sj/lMAAADuElEQVR42u3PQW7DMAwEQP8s//9TAPdYVCBpCnVTtpk92RK1Gh3ntBy/DSAiIiIiIiIiGhYiIiKiKSEiIiKaEiIiIqIpISIiIpoSIiIioikhIiIimhIiIiKiKSEiIiI6j+ssw2tXdmKvmYiIiIiIiIiIiIiIiIiIiIiIiIiIiOg2UWR7F9HjjPP8tqjbTERERERERERERERERERERERERERE9Aiql3MRIQLuNxMRERERERERERERERERERERERERERHdLGr8LmtERERERERERERERERERERERERERERErxctx9JdIiIiIiIiIiIiIiIiIiIiIiIiIiIioh8TBXnWd57JdXvNREREREREREREREREREREREREREREx3W+I2o0/0fR1hcRERERERERERERERERERERERERERHRLaJuoprubqeeiIiIiIiIiIiIiIiIiIiIiIiIiIiIqBJ91hxZouEtZdpHREREREREREREREREREREREREREREtCOKBhuY7ruiESIiIiIiIiIiIiIiIiIiIiIiIiIiIqIdUeSo+6PC+s5GCxEREREREREREREREREREREREREREVEpSi+JWI2HRE+qvURERERERERERERERERERERERERERETXovTOdC0l1KlZRERERERERERERERERERERERERERERKWovriuqYfruXWXiIiIiIiIiIiIiIiIiIiIiIiIiIiI6FJU/y7oYyd1KREREREREREREREREREREREREREREdG2qFGTFjaeRERERERERERERERERERERERERERERHSLaJmuu2pR2pI3ExEREREREREREREREREREREREREREVWibmpg+sJoLhwhIiIiIiIiIiIiIiIiIiIiIiIiIiIiqkTHdaL+umCZa8iJiIiIiIiIiIiIiIiIiIiIiIiIiIiIbhGlrfVN9Rv+kuhxxnkSERERERERERERERERERERERERERG9VnR8TSpa1qLdKGkLERERERERERERERERERERERERERER0c2i5WtJ7U3niIiIiIiIiIiIiIiIiIiIiIiIiIiIiG4WRVmqGl/hLxEREREREREREREREREREREREREREVFPFOS5nFtsDQwREREREREREREREREREREREREREVFXVIeoKWpQI3n6GiIiIiIiIiIiIiIiIiIiIiIiIiIiIqId0ZAQERERTQkRERHRlBARERFNCREREdGUEBEREU0JERER0ZQQERERTQkRERHRlBARERFNCRER0buKPgAt7EaouIFpRAAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxOC0wMy0wNlQxNTo1NzoyMyswMTowMOsnCVkAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTgtMDMtMDZUMTU6NTc6MjMrMDE6MDCaerHlAAAAY3RFWHRzdmc6YmFzZS11cmkAZmlsZTovLy91c3Ivd3d3L3VzZXJzL2JsaXFhbS9xcmNvZGVtb25rZXktYXBpL3RtcC9jNjAyMDQxODIzMjQwNzY2NmRkMzE1ODdmNjgzMGU2OC5zdmfNjZcaAAAAAElFTkSuQmCC'

    # download_images(directory)
    hide_secret(secret, directory)

if __name__ == '__main__':
    main()

