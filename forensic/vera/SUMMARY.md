# Where's Vera?

Vera a disparue, l'équipe Mystery, Inc. recherche désespérément des indices pouvant les aider à savoir où elle est partie.

Seule une clé USB a été retrouvée dans son bureau fermé à clé.

Analyser la clé USB et retrouvez Vera !

<u>**Fichiers&nbsp;:**</u> [vera_usb.tar.gz](files/vera_usb.tar.gz)
