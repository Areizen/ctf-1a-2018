Writeup
=======

Analyse forensic sur une clé USB.

Exploitation&nbsp;:

 - Monter l'image du disque
 - Extraire les données EXIF des fichiers
 - Assembler les données EXIF et recomposer le Base64
 - Décoder le Base64 et scanner le QRCode
 - Monter le volume Veracrypt (`5cr1pt1n6_f0r_th3_W1n`) avec le mot de passe obtenu via le QRCode
 - Lire le flag

Pour l'extraction, il suffit de scripter ça en Python ou Bash (tail, grep, base64).

Flag&nbsp;: `ENSIBS{W3_4ll_10v3_f0r3ns1c!}`
