# What's this?

Un fichier étrange a été retrouvé sur une clé USB appartenant à M. X, principal
suspect dans une affaire de trafic de drogue, vous êtes chargé d'analyser ce fichier.

<u>**Fichier&nbsp;:**</u> [data.txt](files/data.txt)
