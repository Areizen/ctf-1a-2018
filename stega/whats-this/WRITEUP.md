Writeup
=======

Des pixels sont encodés en utilisant un classe de caractère pre-définie (ici [a-zA-Z] -> blanc, [0-9] -> noir).
Pour flag, on doit retrouver l'image ayant été encodée, pour cela, il suffit de tracer les pixels ou bien d'utiliser la fonctionnalité de surlignage dans un éditeur (Atom, par exemple en recherchant le motif [a-zA-Z], on peut lire les données assez facilement).

Exploitation&nbsp;:

```bash
pushd exploit/
pip install -r requirements.txt
python alpha_to_pixel.py
popd
```

Flag&nbsp;: `ENSIBS{l0v3_7h3_w4y_y0u_7h1nk!}`
