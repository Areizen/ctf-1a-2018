Writeup
=======

Dessiner un pixel, déplacer son curseur d'1px dans la direction indiquée, dessiner un nouveau un pixel. Réitérer l'étape jusqu'à la dernière flèche.

Exploitation&nbsp;:

```bash
pushd exploit/
python decode.py
popd
```

Flag&nbsp;: `ENSIBS{HopeYouUsedPython}`
