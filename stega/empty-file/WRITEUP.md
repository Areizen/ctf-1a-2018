Writeup
=======

Le fichier contient le flag, donc en y réflechissant bien, il doit y avoir un moyen de décoder des caractères alphanumériques.

Finalement, il suffit de transformer les espaces en 0 et les tabulations en 1 et enfin convertir la chaîne binaire en ASCII.

Exploitation&nbsp;:

```bash
pushd exploit/
python exploit.py
popd
```

Flag&nbsp;: `ENSIBS{h1DD3n_c0D3}`
