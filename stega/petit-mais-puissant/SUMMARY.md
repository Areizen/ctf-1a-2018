# Petit mais puissant

Un espion est suspecté d'exfiltrer des données sensibles de son entreprise en envoyant des images sur Linkedin.

Analysez l'image de son dernier post et retrouvez l'information exfiltrée.

<u>**Fichier&nbsp;:**</u> [PetitMaisPuissant.png](files/PetitMaisPuissant.png)
