# The Adventure of the Dancing Men

Un inconnu a graffé de curieux symboles d'aspect enfantin sur la façade de votre maison pendant la nuit.

Les symboles dessinés sur votre mur sont-ils simplement des dessins, ou bien ont-ils une signification ? À vous de le découvrir.

<u>**URI&nbsp;:**</u> [http://dancingmen.ensibs.ctf/](http://dancingmen.ensibs.ctf/)