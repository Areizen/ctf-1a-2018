Writeup
=======

Le fichier contient le flag, donc en y réflechissant bien, il doit y avoir un moyen de décoder des caractères alphanumériques.

Finalement, il suffit d'appliquer l'algorithme OTP sur le fichier flag.txt.crypt pour obtenir le flag.

Exploitation&nbsp;:

```bash
pushd exploit/
python decode.py
popd
```

Flag&nbsp;: `ENSIBS{On3_t1m3_p4d}`
