# OTP Really?

Vous avez recupérer un fichier chiffré en en utilisant un OTP.

Par chance vous avez en votre possession un fichier en version claire et chiffrée par le même algorithme.

À vous de déchiffrer le fichier [flag.txt.crypt](files/flag.txt.crypt) pour valider ce challenge.

Bonne chance.

<u>**Fichier&nbsp;:**</u> [flag.txt.crypt](files/flag.txt.crypt) [instruction.txt](files/instruction.txt.crypt) [instruction.txt.crypt](files/instruction.txt.crypt)
