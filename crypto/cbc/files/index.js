'use strict'
//Définition de toute les librairies à importer
const express = require('express');
const crypto = require('crypto');
const fs  = require('fs');
const cookieParser = require('cookie-parser')

//On récupère la clé de chiffrement
const SECRET = fs.readFileSync('KEY.txt','utf-8');
const KEY = crypto.createHash('md5').update(SECRET, 'utf-8').digest('hex').toUpperCase();
const FLAG = fs.readFileSync('FLAG.txt','utf-8');

//Instantiation d'Express.js
var app = express();
app.use(cookieParser())

//Définition du cookie de test :)
var cookie_auth = "admin:0;username:jean-jerome";

//Définition de la route principale
app.get('/',function(req, res){

  // Si le cookie n'existe pas on le défini
  if( req.cookies === undefined || req.cookies.auth === undefined || req.cookies.iv === undefined ){

      // On génère l'IV de façon sécurisée
      var iv  = crypto.randomBytes(16);

      //Instantiation d'un objet permettant de chiffrer l'AES
      var aes = crypto.createCipheriv("aes-256-cbc",KEY,iv);
      var ciphered_data = aes.update(cookie_auth,'utf8','hex') + aes.final('hex');

      console.log(ciphered_data);
      //On définit les cookies à l'utilisateur
      res.cookie("auth",ciphered_data);
      res.cookie("iv", iv.toString("hex") ).send("Cookie set : " + cookie_auth );
  } else {
      //Transformation de l'iv en buffer
      var iv = Buffer.from(req.cookies.iv,"hex");

      // Création de l'instance
      var aes = crypto.createDecipheriv("aes-256-cbc",KEY,iv);
      var auth_value = aes.update(req.cookies.auth,'hex','utf8') + aes.final('utf8');
      var outputvalue = "<p>Current auth value = "+ auth_value + "</p>\n";

      //On vérifie si l'utilisateur est administrateur
      var admin_value = auth_value.split(';')[0].split(':')[1];
      if( admin_value === '1')
      {
        outputvalue += "<p>Welcome back Admin, here is your flag : " + FLAG + "</p>"
        res.send(outputvalue);
      }else {
        outputvalue += "<p>You're not an Admin, go away !</p>";
        res.send(outputvalue);
      }
  }

});

console.log("Serveur launched on port : 8080")
app.listen(8080);
