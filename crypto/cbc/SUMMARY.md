# AES CBC Byte Flipping

Un ami à moi a publié une version de test de son site sur internet et me garantit qu'il est impossible de contourner son système de sécurité.

Trouvez un moyen de contourner son sytème de sécurité par chiffrement de cookie.

Les sources vous sont fournies.

<u>**URI&nbsp;:**</u> [http://cbc.ensibs.ctf/](http://cbc.ensibs.ctf/)
<u>**Fichier&nbsp;:**</u> [index.js](files/index.js)
