Writeup
=======

Une fois connecté sur le site on voit qu'un cookie est défini avec un chiffrement
On possède également l'iv du chiffrement.

L'attaque par Byte Flipping consiste à modifier un byte de l'IV afin de modifier le chiffrement.

Ainsi,
On veut passer `admin=0` en `admin=1`. Pour cela on va modifier le 7ème byte de l'IV afin de modifier la valeur de l'admin.

Ainsi on va soit bruteforcer la valeur du 7 ème byte pour que `admin` vale `1`, soit calculer le xor nécessaire.

Flag&nbsp;: `ENSIBS{XoR-Is-A-Part_of_Bit_flipping}`
