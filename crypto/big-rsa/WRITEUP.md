Writeup
=======

On a e = 3 et n assez grand.
On peut en déduire que si le message n'est pas assez long,
`m^e < n` donc m^e % n = m^e.
Ainsi si on fait une racine troisième de c on obtient le message.

Flag&nbsp;: `ENSIBS{Goliath_RSA_loose_against_David}`
