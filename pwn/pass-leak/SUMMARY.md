# Pass Leak

Des informations sensibles sont cachées sur ce serveur, trouvez un moyen de les faire leaker.

<u>URI&nbsp;:</u> [tcp://pass-leak.ensibs.ctf:31337](tcp:///pass-leak.ensibs.ctf:31337)

<u>Fichiers&nbsp;:</u> [chall](files/chall) [chall.c](files/chall.c)
