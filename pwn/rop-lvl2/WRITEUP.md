Writeup
=======

Exploitation d'une ROP simple avec ROPGadget.

Exploitation&nbsp;:

```bash
pushd exploit/
python exploit.py
cat flag
popd
```

Flag&nbsp;: `ENSIBS{You_saw_it_you_beat_it}`
