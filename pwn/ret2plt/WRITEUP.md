Writeup
=======

Exploitation d'une ROP simple avec ROPGadget.

Exploitation&nbsp;:

```bash
pushd exploit/
python exploit.py
cat flag
popd
```

Flag&nbsp;: `ENSIBS{I_SaW_A_Monster_Last_N1GH_1T_W4s_u}`
}`
