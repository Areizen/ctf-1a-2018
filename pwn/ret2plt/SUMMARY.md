# Ret2Plt

On m'as dit que rien était impossible, là c'est quasiment le cas.
6 lignes de code, un shell ...
Bonne chance !

(Psst ! [Libc-Database](https://github.com/niklasb/libc-database))

<u>URI&nbsp;:</u> [tcp://ret2plt.ensibs.ctf:3127](tcp:///ret2plt.ensibs.ctf:31247)

<u>Fichiers&nbsp;:</u> [chall](files/chall)
