#!/bin/bash

chown 0:1000 data/chall/chall
chown 0:0 data/chall/flag
chmod 4550 data/chall/chall
chmod 400 data/chall/flag

docker-compose pull
docker-compose build
docker-compose up -d

screen -dmS crypto-buffer docker-compose logs -f
screen -ls
