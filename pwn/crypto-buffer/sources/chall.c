#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void chiffrement(char* input);

int main(void)
{

  /* Declaration du buffer d'input */
  char buffer[150];

   /* Message d'accueil */
  printf("Essayez modifier le fil d'exécution du programme sachant que votre entrée est \"cryptée\" :)\n");
  fflush(0);

  /* On récupère l'entrée de façon sécurisée */
  fgets(buffer, sizeof(buffer), stdin);

  /* Methode de chiffrement d'entrée super sécurisée */
  chiffrement(buffer);

  return 0;
}


void chiffrement(char* input)
{
    /* Déclaration des variables */
    char output[100];
    int key = 0x1234;


    /* On génère un xor aléatoire afin de chiffrer la chaîne */
    int rand_var = rand();

    /* On chiffre la chaîne */
    for(int i=0;i<strlen(input);i++){
    	input[i] = input[i] ^ rand_var;
    }

    /* On copie notre chaîne dans la sortie */
    strcpy(output,input);

    /* On affiche la sortie */
    printf("Votre entrée chiffrée : %s\n",&output);

    /* Oh un flag ! ;) */
    if(key == 0xdeadbeef){
    	system("cat ./flag");
    }
}
