# Crypto Buffer

Exploitez ce buffer overflow pour obtenir un flag.

<u>URI&nbsp;:</u> [tcp://crypto-buffer.ensibs.ctf:31234](tcp:///crypto-buffer.ensibs.ctf:31234)

<u>Fichiers&nbsp;:</u> [chall](files/chall) [chall.c](file/chall.c)
