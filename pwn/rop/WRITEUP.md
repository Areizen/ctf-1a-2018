Writeup
=======

Exploitation d'une ROP simple avec ROPGadget.

Exploitation&nbsp;:

```bash
pushd exploit/
python exploit.py
cat flag
popd
```

Flag&nbsp;: `ENSIBS{Y0u_v3_s33n_h0w_toR3ach_my_ropLoplO}`
