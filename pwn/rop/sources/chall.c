#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

/*
clang -o chall chall.c -m32 -fno-stack-protector -g -static
*/
__asm__("pop %eax;ret");

void get_input(void)
{
	char buffer[256];
	gets(buffer);
	return;
}

int main(int argc, char ** argv)
{
	gid_t gid;
 	uid_t uid;
 	gid = getegid();
 	uid = geteuid();

 	setresgid(gid, gid, gid);
 	setresuid(uid, uid, uid);

	get_input();
	return 0;
}
