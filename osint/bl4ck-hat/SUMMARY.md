# Bl4ck Hat

Un espion a infiltré le groupe de l'ENSIBS ! 

Vous le suspecter d'exfiltrer des informations vers un contact hacktiviste.

Menez l'enquète pour découvrir le pot aux roses et l'information exfiltrée auprès de son contact.

Note&nbsp;: 

 - Pas de Stéganographie
 - Pas de Guessing
 - Pas de Bruteforce

Important&nbsp;: il est formellement interdit d'impacter le challenge:

 - Création de fausses preuves
 - Suppressions de preuves existantes

Toute action volontaire ou non pouvant entraver/modifier la résolution du challenge doit être signalée.

En cas de doute, contacter un membre de l'organisation.