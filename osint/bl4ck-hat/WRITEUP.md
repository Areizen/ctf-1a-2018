Writeup
=======

- Trouver sur le groupe Facebook de la classe, "Timothée Brousseau" n'est pas dans la classe
- Extraire le pseudo sur son compte Facebook: "@octarinex"
- Regarder le pseudo sur Twitter: @octarinex
- Extraire sur Twitter sa page github personelle (post "Bruteforcor" https://github.com/Octax/Bruteforcor)
- Sur son projet github perso: un commit a supprimé son mot de passe d'une wordlist
- Se connecter sur son Facebook avec le mot de passe du commit et comme user son id url "timothee.brousseau.12" 
- Solution 2 pour l'user: possibilité de guess le mail via reset passwd.
- Lire les messages privés "en attente"

Flag&nbsp;: `ENSIBS{Insp3ct3ur_G4dg3t}`
