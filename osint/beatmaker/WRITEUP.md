Writeup
=======

- Rechercher PinkOtter sur soundcloud: https://soundcloud.com/user-134011939
- Télécharger sa musique "Trying to Progress"
- Extraire les tags de sa musique : exiftool "Trying to Progress.mp3"
- Lire "User Defined Text: (PATH) C:\Users\EglantineGingras\Music\"
- Calculer md5(GINGRAS) EN MAJUCULE !
- MD5 = b2f7340bfb57d4a7dcdf49934d2f97de

Flag&nbsp;: `ENSIBS{b2f7340bfb57d4a7dcdf49934d2f97de}`
