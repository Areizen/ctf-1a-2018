# BeatMaker

Le fameux beatmaker "PinkOtter" cache sa réelle identité, sauriez-vous la retrouver ?

Pour valider ce challenge, vous devez retrouver son nom de famille et valider avec&nbsp;: 
ENSIBS{md5(NOMDEFAMILLE)}

Par exemple si la personne s'appelait Jean Dupond, le flag serait ENSIBS{aed5b2c5acafd128f2ca0ee870e11982}

Note&nbsp;: 

 - Pas de Stéganographie
 - Pas de Guessing
 - Pas de Bruteforce

Important&nbsp;: il est formellement interdit d'impacter le challenge:

 - Création de fausses preuves
 - Suppressions de preuves existantes

Toute action volontaire ou non pouvant entraver/modifier la résolution du challenge doit être signalée.

En cas de doute, contacter un membre de l'organisation.