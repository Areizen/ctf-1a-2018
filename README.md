# Liste des services

| Catégorie | Frontend                                               | Backend port |
|-----------|--------------------------------------------------------|--------------|
| Pwn       | [Ret2PLT](tcp://ret2plt.ensibs.ctf:31247)              | `31247/tcp`  | 
| Pwn       | [ROP2](tcp://rop2.ensibs.ctf:31246)                    | `31246/tcp`  |     
| Pwn       | [Crypto Buffer](tcp://crypto-buffer.ensibs.ctf:31234/) | `31234/tcp`  |
| Pwn       | [Pass Leak](tcp://pass-leak.ensibs.ctf:31337/)         | `31337/tcp`  |
| Pwn       | [ROP](tcp://rop.ensibs.ctf:31245/)                     | `31245/tcp`  |
| Misc      | [Welcome](tcp://welcome.ensibs.ctf:42137/)             | `42137/tcp`  |
| Misc      | [Not So White LVL1](tcp://notsowhite.ensibs.ctf:7770/) | `7770/tcp`   |
| Misc      | [Not So White LVL2](tcp://notsowhite.ensibs.ctf:7771/) | `7771/tcp`   |
| Misc      | [Not So White LVL3](tcp://notsowhite.ensibs.ctf:7772/) | `7772/tcp`   |
| Misc      | [Not So White LVL4](tcp://notsowhite.ensibs.ctf:7773/) | `7773/tcp`   |
| Misc      | [Not So White LVL5](tcp://notsowhite.ensibs.ctf:7774/) | `7774/tcp`   |
| Web       | [ElectronJS](http://electronjs.ensibs.ctf/)            | `8080/tcp`   |
| Web       | [HaRPOn](http://harpoon.ensibs.ctf/)                   | `8081/tcp`   |
| Web       | [Ultimate Hash Algorithm](http://hash.ensibs.ctf/)     | `8082/tcp`   |
| Web       | [JWaTson](http://jwatson.ensibs.ctf/)                  | `8083/tcp`   |
| Web       | [My Object](http://myobject.ensibs.ctf/)               | `8084/tcp`   |
| Web       | [Serial Killer](http://serialkiller.ensibs.ctf/)       | `8085/tcp`   |
| Crypto    | [Dancing Men](http://dancingmen.ensibs.ctf/)           | `8086/tcp`   |
| Crypto    | [CBC Byte Flipping](http://cbc.ensibs.ctf/)            | `8087/tcp`   |
| Web       | [MyGame](http://mygame.ensibs.ctf/)                    | `8088/tcp`   |
| Web       | [ZooCheck](http://zoocheck.ensibs.ctf/)                | `8089/tcp`   |

## Setup bridge

```bash
docker network create \
       --driver bridge \
       --subnet=10.2.0.0/24 \
       --ip-range=10.2.0.0/24 \
       --gateway=10.2.0.254 \
       --opt "com.docker.network.bridge.default_bridge"="false" \
       --opt "com.docker.network.bridge.enable_icc"="true" \
       --opt "com.docker.network.bridge.enable_ip_masquerade"="true" \
       --opt "com.docker.network.bridge.name"="br2" \
       --opt "com.docker.network.driver.mtu"="1500" \
       middle
docker network create \
       --driver bridge \
       --subnet=10.1.0.0/24 \
       --ip-range=10.1.0.0/24 \
       --gateway=10.1.0.254 \
       --internal=true \
       --opt "com.docker.network.bridge.default_bridge"="false" \
       --opt "com.docker.network.bridge.enable_icc"="true" \
       --opt "com.docker.network.bridge.enable_ip_masquerade"="false" \
       --opt "com.docker.network.bridge.name"="br1" \
       --opt "com.docker.network.driver.mtu"="1500" \
       back
docker network create \
       --driver bridge \
       --subnet=10.0.0.0/24 \
       --ip-range=10.0.0.0/24 \
       --gateway=10.0.0.254 \
       --opt "com.docker.network.bridge.default_bridge"="false" \
       --opt "com.docker.network.bridge.enable_icc"="true" \
       --opt "com.docker.network.bridge.enable_ip_masquerade"="true" \
       --opt "com.docker.network.bridge.name"="br0" \
       --opt "com.docker.network.driver.mtu"="1500" \
       front

```
