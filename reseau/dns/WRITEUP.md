Writeup
=======

Déchiffrer les données à partir de la clé privée extraite du pcap.

Exploitation&nbsp;:

```bash
pushd exploit/
./writeup.sh
popd
```

Flag&nbsp;: `ENSIBS{Exf1ltr@ti0n_8y_DNS_7unne1in9}`
