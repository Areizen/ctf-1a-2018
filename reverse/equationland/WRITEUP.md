Writeup
=======

Pour valider ce challenge il faut soit résoudre les équations, soit utiliser un SAT solveur pour le résoudre.

Flag&nbsp;: `ENSIBS{you_solved_it}`
