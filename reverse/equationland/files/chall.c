#include <stdio.h>
#include <string.h>
#include <sys/ptrace.h>

int main(int argc,char *argv[])
{
	if(argc < 2 && strlen(argv[1]) != 13 ){
		return 1;
	}


	int len = 13;
	char * input = argv[1];

	if(ptrace(PTRACE_TRACEME, 0,1, 0) == -1)
	{
		printf("I see that you're using a debugger\n");
		return 1;
	}

	if(input[12] != 116)
	{
		return 1;
	}

	if(input[1] != 123-12)
	{
		return 1;
	}

	if(input[3] != input[0] + input[6] + input[2]-244 - 7)
	{
		return 1;
	}

	if(input[0] != (134 - len))
	{
		return 1;
	}

	if(input[3] - input[7] != -23)
	{
		return 1;
	}

	if(input[7] != input[8] + 17)
	{
		return 1;
	}

	if(input[8] != 101 + input[7] - 118)
	{
		return 1;
	}

	if(input[6] - 107 != input[7] - input[2])
	{
		return 1;
	}

	if(input[6] + input[5] != input[3] + input[8] + 23 )
	{
		return 1;
	}


	if(input[9] - 203 != len - input[12])
	{
		return 1;
	}

	if( input[10] - input[12] != -21 )
	{
		return 1;
	}

	if( input[11] != 105){
		return 1;
	}

	if(input[7]+input[9] != 218){
		return 1;
	}

	if(input[6] != input[7]+input[9]-110){
		return 1;
	}
	if(input[0]+input[1]+input[2]+input[3]+input[4]+input[5]+input[6]+input[7]+input[8]+input[9]+input[10]+input[11]+input[12] != 1413){
		return 1;
	}

	printf("Well done : ENSIBS{%s}\n",input);
	return 0;
}
