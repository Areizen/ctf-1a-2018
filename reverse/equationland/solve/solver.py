#!/bin/python3
from z3 import *

s = Solver()
n = 13
a = IntVector('a',n)

s.add( a[12] == 116)
s.add(a[1] == 123 - 12)
s.add(a[3] == a[0] + a[6] + a[2]-244 - 7)
s.add( a[0] == 134-n)
s.add(a[3] - a[7] == -23 )
s.add( a[7] == a[8] + 17)
s.add(a[8] == 101 + a[7] - 118 )
s.add( a[6] - 107 == a[7] - a[2])
s.add( a[6] + a[5] == a[3] + a[8] + 23)
s.add( a[9] - 203 == n - a[12])
s.add( a[10] - a[12] == -21)
s.add( a[11] == 105 )
s.add(a[7]+a[9] == 218)
s.add(a[6] == a[7]+a[9]-110)
s.add(a[0]+a[1]+a[2]+a[3]+a[4]+a[5]+a[6]+a[7]+a[8]+a[9]+a[10]+a[11]+a[12] == 1413)

while s.check() == sat :
    m = s.model()
    s.add( Or(a[0] != m[a[0]] , a[1] != m[a[1]] , a[2] != m[a[2]], a[3] != m[a[3]], a[4] != m[a[4]], a[5] != m[a[5]], a[6] != m[a[6]], a[7] != m[a[7]] , a[8] != m[a[8]], a[9] != m[a[9]], a[10] != m[a[10]], a[11] != m[a[11]], a[12] != m[a[12]]) )
    for f in a :
        if m[f]!= None:
            print(chr(m[f].as_long()))
