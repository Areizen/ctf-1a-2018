Writeup
=======

- reversed(flag[-16:-1][::-1]) retire le substitut "ENSIBS{" et "}"
- j = 4 et on voit l'opération xor "^" avec verif[j], on en déduit que le pass est un xor de la vérification avec verif[4:]
- On test l'opération ((ord(elt) & 0b10101010) + (ord(elt) & 0b1010101)), on se rend compte que celle-ci est identique à ord(elt)
- On a donc l'opération ord(elt) = verif[4:] ^ ord(chaine de verification)
- On effectue le xor, et on récupère les ord correspondant à la chaine "Eunectes!Boidae"
- On reprend le programme de base en remplacant XXX par Eunectes!Boidae, le programme indique que le mot de passe est valide.

Flag&nbsp;: `ENSIBS{Eunectes!Boidae}`
