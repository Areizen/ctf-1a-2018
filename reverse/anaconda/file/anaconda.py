#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys


flag = "ENSIBS{XXX}" # Insert Here

verif = [127,39,99,41,50,120,0,255,12,45,78,35,155,98,52,78,45,68,5,70,65,32]




# Few verification, no need to reverse this
if (sys.version_info < (3, 0)): # If Python 2
    print("Python 3 is required !") # Kill process
    sys.exit(0)
if flag[:7] != "ENSIBS{" or flag[-1] != "}": # If flag doesn't contain ENSIBS{}
    print("ENSIBS{} is required !") # Kill process
    sys.exit(0)
# End of verification



x = reversed(flag[-16:-1][::-1])
s = ""
j = 4
for elt in ''.join(x):
    j += 1
    s += chr(((ord(elt) & 0b10101010) + (ord(elt) & 0b1010101)) ^ verif[j])


if s == "=uiN:FèCv!D d#":
    print("Well done ! You can flag with: "+flag)
else:
    print("Wrong Password :( !")
    print(s)
