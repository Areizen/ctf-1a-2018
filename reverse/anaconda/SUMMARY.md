# Anaconda

Un développeur novice a décidé d'obfusquer son code python afin de ne pas afficher son mot de passe en clair.
Sauriez-vous le retrouver ?

Note&nbsp;: Ce reverse est un reverse Python3, et ne fonctionne pas sous Python2.

<u>**Fichier&nbsp;:**</u> [anaconda.py](files/anaconda.py)