Writeup
=======

Exploitation d'une [RPO](https://www.bmoine.fr/#rpo) simple via *file upload*, la plus grosse difficulté est que la vulnérabilité n'est pas encore très connue.

Aperçu de la vulnérabilité&nbsp;: https://gitlab.com/Areizen/CTF-1A-2017/blob/master/web/harpoon/exploit/HaRPOon.pdf

Exploitation&nbsp;:

```bash
pushd exploit/
pip install -r requirements.txt
python exploit.py
popd
```

Flag&nbsp;: `ENSIBS{51mpl3_1s_0ft3n_g00d}`
