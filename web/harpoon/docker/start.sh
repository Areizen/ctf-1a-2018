#!/bin/bash

chown -R 33:33 ./data/www/webroot/uploads/

apparmor_parser ./apparmor.profile

docker-compose pull
docker-compose build
docker-compose up -d

screen -dmS harpoon docker-compose logs -f
screen -ls
