SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

USE db;

DROP TABLE IF EXISTS urls;
CREATE TABLE urls (
  id INT(11) NOT NULL AUTO_INCREMENT,
  url VARCHAR(255) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (url)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO urls (id, url) VALUES
(1, 'http://harpoon.ensibs.ctf/uploads/74d99c0e22fabfbe669fe7aedb196ee0/..%2F..%2F');
