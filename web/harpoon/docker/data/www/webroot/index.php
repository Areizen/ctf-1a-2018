<?php
    ob_start();
    require_once("include/session.php");
    require_once("include/config.php");
    require_once("include/header.php");
?>
        <div id="main">
            <div class="wrapper">
                <div class="row">
                    <div class="col-l-12 col-m-12 col-s-12">
                        <h2>Welcome to <?php echo APP_NAME ?>!</h2>
                        <hr />
                        <p><?php echo APP_DESCRIPTION ?></p>
                        <a href="upload.php" id="scan" title="Begin scan">Get started</a>
                    </div>
                </div>
            </div>
        </div>
<?php
    require_once("include/footer.php");
    ob_end_flush();
