<?php
    ob_start();
    require_once("include/session.php");
    require_once("include/config.php");
    require_once("include/header.php");
?>
        <div id="main">
            <div class="wrapper">
                <div class="row">
                    <div class="col-l-12 col-m-12 col-s-12">
                        <h2>Make your CSS great again</h2>
                        <ol>
                            <li>Upload your CSS file</li>
                            <li>Wait for the sanitization process to finish</li>
                            <li>Enjoy your CSS on every platform!</li>
                        </ol>
                        <p>Please note that the application is currently
                          undergoing massive development, the sanitization
                          service isn't yet working.</p>
                    </div>
                </div>
            </div>
        </div>
<?php
    require_once("include/footer.php");
    ob_end_flush();
