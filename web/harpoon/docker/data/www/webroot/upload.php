<?php
    ob_start();
    require_once("include/session.php");
    require_once("include/config.php");
    require_once("include/header.php");
?>
        <div id="main">
            <div class="wrapper">
                <div class="row">
                    <div class="col-l-12 col-m-12 col-s-12">
<?php
    if ((isset($_POST)) && ($_SERVER["REQUEST_METHOD"] == "POST")) {
?>
                        <p>
<?php
        // Process POST request
        if (isset($_FILES["file"])) {
            if ($_FILES["file"]["error"] == 0) {
                $file_name = $_FILES["file"]["name"];  // Get original filename
                $file_size = $_FILES["file"]["size"];  // Get filesize
                $file_temp = $_FILES["file"]["tmp_name"];  // Get temporary filename
                $file_info = pathinfo($file_name);  // Array containing filename info
                $file_extension = strtolower($file_info["extension"]); // Get image extension from filename

                if (isset($_POST["upload"])) {  // Upload directory
                    $pattern = "/^uploads\/" . session_id() . "\/.*$/";
                    if (preg_match($pattern, $_POST["upload"])) {  // Check upload directory
                        $upload_dir = $_POST["upload"];
                    } else {
                        $upload_dir = "uploads/" . session_id() . "/";
                    }
                } else {
                    $upload_dir = "uploads/" . session_id() . "/";
                }

                if (! is_dir($upload_dir)) {
                    mkdir($upload_dir, 0755, true);
                }

                if ($file_size < 104857600) {  // If filesize is lower than 100MB
                    if (move_uploaded_file($file_temp, $upload_dir . $file_name)) {  // Move file to upload directory
                        echo("Your file has successfully been uploaded to <a href=\"" . $upload_dir . $file_name . "\" title=\"upload file\">". $upload_dir . $file_name . "</a>.");
                    }
                } else {
                    echo("Your file exceeds file size limit of 100 MB!");
                    header("400 Bad Request", true, 400);
                }
            } else {
                echo("An error occured while processing your file!");
                header("500 Internal Server Error", true, 500);
            }
        } else {
            echo("Please send a file!");
            header("400 Bad Request", true, 400);
        }
?>
                       </p>
<?php
    } elseif ((isset($_GET)) && ($_SERVER["REQUEST_METHOD"] == "GET")) {
?>
                        <h2>Upload your CSS file</h2>
                        <form action="upload.php" method="POST" enctype="multipart/form-data">
                            <fieldset>
                                <input type="file" name="file" required="required" />
                                <input type="hidden" name="upload" value="uploads/<?php echo session_id(); ?>/" />
                            </fieldset>
                            <fieldset>
                                <input type="submit" value="Upload" />
                            </fieldset>
                        </form>
<?php
    } else {
        // Method Not Allowed
        echo("Method Not Allowed");
        header("Method Not Allowed", true, 405);
    }
?>
                    </div>
                </div>
            </div>
        </div>
<?php
    require_once("include/footer.php");
    ob_end_flush();