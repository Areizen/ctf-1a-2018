<?php
    ob_start();
    require_once("include/session.php");
    require_once("include/config.php");
    require_once("include/db.php");

    if ((isset($_GET["admin"])) && $_GET["admin"] == ADMIN_TOKEN) {
        try {
            $statement = $db->prepare("SELECT id, url FROM urls");

            $statement->execute();
            $results = $statement->fetchAll(PDO::FETCH_ASSOC);
            $statement->closeCursor();

            foreach ($results as $url) {
                $statement = $db->prepare("DELETE FROM urls WHERE id = :id");

                $statement->bindParam(":id", $url['id'], PDO::PARAM_INT);
                $statement->execute();
                $statement->closeCursor();
            }

            echo(json_encode($results));
            header("Content-type: application/json; charset=UTF-8");
        } catch (Exception $e) {
            echo("An error occured while trying to get URL from database. Please try again...");
        }
    } else {
        require_once("include/header.php");
?>
        <div id="main">
            <div class="wrapper">
                <div class="row">
                    <div class="col-l-12 col-m-12 col-s-12">
<?php
        if ((isset($_POST)) && ($_SERVER["REQUEST_METHOD"] == "POST")) {
?>
                        <p>
<?php
            // Process POST request
            if (isset($_POST["url"])) {
                $pattern = "/^" . APP_BASE_URI_PATTERN . "uploads\/" . session_id() . "\/.*$/";
                if (preg_match($pattern, $_POST["url"])) {  // Check upload directory
                    try {
                        $statement = $db->prepare("INSERT INTO urls (url) VALUES ( :url )");

                        $statement->bindParam(":url", $_POST["url"], PDO::PARAM_STR);
                        $statement->execute();
                        $statement->closeCursor();

                        echo("Your URL has successfully been submitted.");
                    } catch (Exception $e) {
                        echo("An error occured while trying to submit your URL. Please try again...");
                    }
                } else {
                    echo("Please check your URL!");
                    header("400 Bad Request", true, 400);
                }
            } else {
                echo("Please send a URL!");
                header("400 Bad Request", true, 400);
            }
?>
                       </p>
<?php
        } elseif ((isset($_GET)) && ($_SERVER["REQUEST_METHOD"] == "GET")) {
?>
                        <h2>Reporting an issue</h2>
                        <p>In order to ensure our services are running as
                          flawlessly as possible, if you encounter a technical
                          issue, please send us the URL of the downloaded file.
                          An administrator will check your file and will fix it
                          as soon as possible (usually within 5 minutes, please
                          check your report before sending!).</p>
                    </div>
                    <div class="col-l-12 col-m-12 col-s-12">
                        <form action="report.php" method="POST">
                            <fieldset>
                                <input type="url" name="url" placeholder="<?php echo(APP_BASE_URI); ?>uploads/<?php echo(session_id()); ?>/" pattern="<?php echo(APP_BASE_URI_PATTERN_DOM); ?>uploads/<?php echo(session_id()); ?>/.*$" value="<?php echo(APP_BASE_URI); ?>uploads/<?php echo(session_id()); ?>/" />
                            </fieldset>
                            <fieldset>
                                <input type="submit" value="Report" />
                            </fieldset>
                        </form>
<?php
        } else {
            // Method Not Allowed
            echo("Method Not Allowed");
            header("Method Not Allowed", true, 405);
        }
?>
                    </div>
                </div>
            </div>
        </div>
<?php
        require_once("include/footer.php");
    }
    ob_end_flush();
