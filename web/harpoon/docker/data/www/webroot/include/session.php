<?php
    ini_set('session.use_strict_mode', 1);  // prevent usage of uninitialized session id
    session_name("session");
    session_start();
