<?php
    define("DB_HOST", "db");
    define("DB_NAME", "db");
    define("DB_USER", "user");
    define("DB_PASS", "ch4113ng3_p455w0rd_my5q1");

    define("APP_NAME", "HaRPOon");
    define("APP_HOST", "harpoon.ensibs.ctf");
    define("APP_BASE_URI", "http://" . APP_HOST . "/");
    define("APP_BASE_URI_PATTERN", "http:\/\/harpoon\.ensibs\.ctf\/");
    define("APP_BASE_URI_PATTERN_DOM", "http://harpoon\.ensibs\.ctf/");
    define("APP_DESCRIPTION", "A powerful CSS sanitization service");
    define("APP_AUTHOR", "Creased");

    define("ADMIN_TOKEN", "5f28cdb912c5dd9fbdf751C25848FE2B028963D6F85B001287AEDC78089ec665");

    define("FLAG", "ENSIBS{51mpl3_1s_0ft3n_g00d}");
