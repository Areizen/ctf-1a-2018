#!/usr/bin/env python
# -*- coding:Utf-8 -*-

# Author: Baptiste MOINE <contact@bmoine.fr>.

"""Bot."""

# Generate documentation: epydoc -v --html main.py -o ./docs

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import requests
import json
import time

from packages import log

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '06 February 2017'

def load_url(url, host, admin_token):
    ##
    # HTTP Request Content
    #
    method = 'GET'  # HTTP method

    headers = {      # HTTP headers
        'Host':             host,
        'User-Agent':       'Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0',
        'Accept':           'application/json',
        'Accept-Language':  'en-US,en;q=0.5',
        'Accept-Encoding':  'gzip, deflate',
        'Connection':       'close'
    }

    ##
    # HTTP Request
    #
    req = requests.Request(  # Create the HTTP request with data and headers
        method,
        '{url}?admin={token}'.format(url=url, token=admin_token),
        headers=headers
    )
    res = requests.Session()        # Create an HTTP session
    req = res.prepare_request(req)  # Prepare the request for sending

    ##
    # HTTP Response
    #
    res = res.send(  # Send the prepared HTTP request
        req,
        verify=False,
        timeout=None,
        allow_redirects=True
    )

    result = json.loads(res.text)

    return result

while True:
    try:
        admin_token = '5f28cdb912c5dd9fbdf751C25848FE2B028963D6F85B001287AEDC78089ec665'
        host = 'harpoon.ensibs.ctf'
        report_url = 'http://{host}/report.php'.format(host=host)
        home_url = 'http://{host}/'.format(host=host)

        urls = load_url(report_url, host, admin_token)

        if len(urls) > 0:
            try:
                log.info('{count} URL to process'.format(count=len(urls)))
                log.info('Loading Firefox Driver...')
                driver = webdriver.Remote(command_executor='http://selenium:4444/wd/hub',
                                          desired_capabilities=DesiredCapabilities.FIREFOX)
                # driver = webdriver.Firefox()
                driver.get('{url}?admin={token}'.format(url=home_url, token=admin_token))

                for url in urls:
                    log.info('Opening: {}'.format(url['url']))
                    driver.get(url['url'])
                    time.sleep(10)  # wait 10 seconds
            except Exception as e:
                print(e)

            driver.quit()

        time.sleep(5)
    except Exception as e:
        print(e)