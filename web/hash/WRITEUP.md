Writeup
=======

L'algorithme est un chiffrement de Caesar en ROT18 sur une base de 36 (lettres minuscules + chiffres).

Donc si on donne en mot de passe le hash on récupére le flag.

Le mot de passe est donc "n3v3r_cre4t3_y0ur_0wn_h4sh_4lg0"

Flag&nbsp;: `ENSIBS{n3v3r_cre4t3_y0ur_0wn_h4sh_4lg0}`

