# JWaTson

John H. Watson vient de lancer son blog afin de partager ses aventures avec le grand Sherlock Holmes.

N'ayant que très peu de temps à consacrer au développement de son site, il a confié les droits d'administrations à des administrateurs qui sont les seuls à pouvoir consulter et modifier les articles pour le moment.

Mandaté par l'inspecteur G. Lestrade, vous êtes chargés de trouver un moyen d'accéder aux articles déjà publiés.

<u>**URI&nbsp;:**</u> [http://jwatson.ensibs.ctf/](http://jwatson.ensibs.ctf/)
