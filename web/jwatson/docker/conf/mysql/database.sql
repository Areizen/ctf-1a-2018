SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

USE db;

DROP TABLE IF EXISTS users;
CREATE TABLE users (
  id INT(11) NOT NULL AUTO_INCREMENT,
  user VARCHAR(255) NOT NULL,
  password VARCHAR(255) NOT NULL,
  role VARCHAR(255) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (user)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO users (id, user, password, role) VALUES
(1, 'admin', '$2y$10$dGlOz7njyXX2zI05YPwbZ.klI4Z2jSzuSwTqrqvQEj5qtRQfKYin6', 'admin'),  # eW91IGRvbid0IG5lZWQgaXQg
(2, 'user', '$2y$10$Ebyg19E9Iu4mkaUppRIKHeay7PPbdUpGvwoUJ84A3coyvEBo0Wv0S', 'user');  # password
