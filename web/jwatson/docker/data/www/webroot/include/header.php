<?php
    if ((isset($_GET["admin"])) && $_GET["admin"] == ADMIN_TOKEN) {
        setcookie("flag", FLAG, time() + 7200, "/", APP_HOST, 0);
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>

        <title><?php echo APP_NAME ?> &#8211; <?php echo APP_DESCRIPTION ?></title>

        <link rel="icon" href="images/favicon.ico" type="image/x-icon">

        <link rel="stylesheet" media="all" href="css/main.css">
        <link rel="stylesheet" media="all" href="css/fontello.css">
    </head>
    <body>
        <div id="header">
            <div class="wrapper">
                <div class="row">
                    <div class="col-l-7 col-m-7 col-s-12">
                        <h1 class="page-header">{ <?php echo APP_NAME ?> } <span class="small">by <?php echo APP_AUTHOR ?></span></h1>
                    </div><!--
                    --><div id="nav" class="col-l-5 col-m-5 col-s-12">
                        <ul>
                            <li><a href="index.php">Home</a></li><!--
                            --><li><a href="about.php">About</a></li><!--
                            --><li><a href="access.php">Access</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
