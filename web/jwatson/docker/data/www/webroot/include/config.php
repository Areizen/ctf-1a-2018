<?php
    define("DB_HOST", "db");
    define("DB_NAME", "db");
    define("DB_USER", "user");
    define("DB_PASS", "ch4113ng3_p455w0rd_my5q1");

    define("APP_NAME", "JWaTson");
    define("APP_HOST", "jwatson.ensibs.ctf");
    define("APP_BASE_URI", "http://" . APP_HOST . "/");
    define("APP_BASE_URI_PATTERN", "http:\/\/jwatson\.ensibs\.ctf\/");
    define("APP_BASE_URI_PATTERN_DOM", "http://jwatson\.ensibs\.ctf/");
    define("APP_DESCRIPTION", "The personal blog of Dr. John H. Watson");
    define("APP_AUTHOR", "Creased");

    define("JWT_KEY", "221bbs");
    define("JWT_ALG", array("HS256"));
    define("FLAG", "ENSIBS{7h3_5c13Nc3_0f_d3DuC710n}");
