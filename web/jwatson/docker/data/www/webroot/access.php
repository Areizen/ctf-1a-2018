<?php
  ob_start();
  require_once("include/config.php");
  require_once("include/db.php");
  require_once("include/jwt/JWT.php");
  use \Firebase\JWT\JWT;
  require_once("include/header.php");
?>
    <div id="main">
        <div class="wrapper">
            <div class="row">
                <div class="col-l-12 col-m-12 col-s-12">
                    <p>
<?php
  class User {
    protected $name;
    protected $role;

    public function __construct($name, $role) {
      $this->name = $name;
      $this->role = $role;
    }

    public function __toString() {
      return $this->name;
    }

    public function getToken() {
        return array('user' => $this->name, 'role' => $this->role);
    }

    public function getAccess() {
      if ($this->role === "admin") {
        return 1;
      } else {
        return 0;
      }
    }
  }

  if (isset($_COOKIE["session"]) && !empty($_COOKIE["session"])) {
    try {
      $cookie_data = JWT::decode(base64_decode($_COOKIE["session"]), JWT_KEY, JWT_ALG);
      $user = new User($cookie_data->user, $cookie_data->role);
    } catch (Error | Exception $e) {
        header("400 Bad Request", true, 400);
        echo($_COOKIE["session"]);
        echo("Sorry an error occured while trying to decode your token.");
    }
  } else {
    if ($_SERVER["REQUEST_METHOD"] === "POST") {
      if (isset($_POST["user"]) && !empty($_POST["user"]) && isset($_POST["password"]) && !empty($_POST["password"])) {
        $statement = $db->prepare("SELECT user, password, role FROM users WHERE user = :user");

        $statement->bindParam(":user", $_POST['user'], PDO::PARAM_STR);
        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();

        if ($results) {
          $password = $results[0]['password'];
          $role = $results[0]['role'];
          if (password_verify($_POST["password"], $password)) {
            $user = new User($_POST["user"], $role);
            try {
              $cookie_data = base64_encode(JWT::encode($user->getToken(), JWT_KEY));
              setcookie("session", $cookie_data, time()+3600, "/", "", 0);
            } catch (Error | Exception $e) {
                header("400 Bad Request", true, 400);
                echo("Sorry an error occured while trying to process your credentials.");
            }
          } else {
            echo("Invalid credentials!");
            header("400 Bad Request", true, 400);
          }
        } else {
          echo("Invalid credentials!");
          header("400 Bad Request", true, 400);
        }
      } else {
        echo("Invalid credentials!");
        header("400 Bad Request", true, 400);
      }
    }
  }

  if (isset($user)) {
    if ($user->getAccess()) {
      echo("Access Granted! Welcome " . $user . "!\n");
      echo(FLAG);
    } else {
      echo("Access denied to password database (not an admin member)!\n");
    }
?>
                    </p>
<?php
  } else {
?>
                  </p>
                  <form action="access.php" method="POST">
                      <fieldset>
                          <input type="text" name="user" placeholder="Username" pattern="^.+$" value="" />
                          <input type="password" name="password" placeholder="Password" pattern="^.+$" value="" />
                      </fieldset>
                      <fieldset>
                          <input type="submit" value="Login" />
                      </fieldset>
                  </form>
<?php
  }
?>
                </div>
            </div>
        </div>
    </div>
<?php
  require_once("include/footer.php");
  ob_end_flush();
