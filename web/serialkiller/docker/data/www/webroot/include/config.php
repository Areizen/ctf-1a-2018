<?php
    define("DB_HOST", "db");
    define("DB_NAME", "db");
    define("DB_USER", "user");
    define("DB_PASS", "ch4113ng3_p455w0rd_my5q1");

    define("APP_NAME", "Serial Killer");
    define("APP_HOST", "serialkiller.ensibs.ctf");
    define("APP_BASE_URI", "http://" . APP_HOST . "/");
    define("APP_BASE_URI_PATTERN", "http:\/\/serialkiller\.ensibs\.ctf\/");
    define("APP_BASE_URI_PATTERN_DOM", "http://serialkiller\.ensibs\.ctf/");
    define("APP_DESCRIPTION", "A powerful password database service");
    define("APP_AUTHOR", "Creased");

    define("FLAG", "ENSIBS{d0n'7_53r14l1z3_u53r_d474!}");
