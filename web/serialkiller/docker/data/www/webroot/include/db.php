<?php
    try {
        $db = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . ";charset=utf8", DB_USER, DB_PASS);
    } catch (Exception $e) {
        die("An error occured while trying to connect to backend database.");
    }
