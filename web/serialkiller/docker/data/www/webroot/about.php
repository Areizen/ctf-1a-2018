<?php
    ob_start();
    require_once("include/config.php");
    require_once("include/header.php");
?>
        <div id="main">
            <div class="wrapper">
                <div class="row">
                    <div class="col-l-12 col-m-12 col-s-12">
                        <h2>Make your password policy harder!</h2>
                        <p>Demo: <b>user</b> / <b>password</b></p>
                        <p>Please note that the application is currently
                          undergoing massive development. Password database
                          is only available to admin users.</p>
                    </div>
                </div>
            </div>
        </div>
<?php
    require_once("include/footer.php");
    ob_end_flush();
