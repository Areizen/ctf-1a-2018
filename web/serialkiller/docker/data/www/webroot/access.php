<?php
  ob_start();
  require_once("include/config.php");
  require_once("include/db.php");
  require_once("include/header.php");
?>
    <div id="main">
        <div class="wrapper">
            <div class="row">
                <div class="col-l-12 col-m-12 col-s-12">
                    <p>
<?php
  class Log {
    protected $file;

    public function __construct() {
      $this->file = sys_get_temp_dir() . "/sk-log";
    }

    public function write($data) {
      file_put_contents($this->file, $data . PHP_EOL, FILE_APPEND);
    }

    public function __toString() {
      return file_get_contents($this->file);
    }
  }

  class User {
    protected $name;
    protected $role;

    public function __construct($name, $role) {
      $this->name = $name;
      $this->role = $role;
    }

    public function __toString() {
      return $this->name;
    }

    public function getAccess() {
      if ($this->role === "admin") {
        return 1;
      } else {
        return 0;
      }
    }
  }

  $log = new Log();

  if (isset($_COOKIE["session"]) && !empty($_COOKIE["session"])) {
    $user = unserialize(base64_decode($_COOKIE["session"]));
  } else {
    if ($_SERVER["REQUEST_METHOD"] === "POST") {
      if (isset($_POST["user"]) && !empty($_POST["user"]) && isset($_POST["password"]) && !empty($_POST["password"])) {
        $statement = $db->prepare("SELECT user, password, role FROM users WHERE user = :user");

        $statement->bindParam(":user", $_POST['user'], PDO::PARAM_STR);
        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        $statement->closeCursor();

        if ($results) {
          $password = $results[0]['password'];
          $role = $results[0]['role'];
          if (password_verify($_POST["password"], $password)) {
            $user = new User($_POST["user"], $role);
            setcookie("session", base64_encode(serialize($user)), time()+3600, "/", "", 0);
          } else {
            echo("Invalid credentials!");
            header("400 Bad Request", true, 400);
          }
        } else {
          echo("Invalid credentials!");
          header("400 Bad Request", true, 400);
        }
      } else {
        echo("Invalid credentials!");
        header("400 Bad Request", true, 400);
      }
    }
  }

  if (isset($user)) {
    try {
      if ($user->getAccess()) {
        echo("Access Granted! Welcome " . $user . "!\n");
        $log->write("Access granted to " . $user);
      } else {
        echo("Access denied to password database (not an admin member)!\n");
      }
    } catch (Error | Exception $e) {
        echo("Sorry " . $user . ", an error occured while trying to get access to the system.");
    }
?>
                    </p>
<?php
  } else {
?>
                  </p>
                  <form action="access.php" method="POST">
                      <fieldset>
                          <input type="text" name="user" placeholder="Username" pattern="^.+$" value="" />
                          <input type="password" name="password" placeholder="Password" pattern="^.+$" value="" />
                      </fieldset>
                      <fieldset>
                          <input type="submit" value="Login" />
                      </fieldset>
                  </form>
<?php
  }
?>
                </div>
            </div>
        </div>
    </div>
<?php
  require_once("include/footer.php");
  ob_end_flush();
