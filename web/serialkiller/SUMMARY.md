# Serial Killer

Un service de coffre fort de mot de passe est en cours de développement.

Prouvez aux développeurs qu'ils ont encore du travail devant eux pour sécuriser pleinement leur application en obtenant l'accès au compte de l'administrateur.

<u>**URI&nbsp;:**</u> [http://serialkiller.ensibs.ctf/](http://serialkiller.ensibs.ctf/)

<u>**Sources&nbsp;:**</u> [serialkiller.tar.gz](serialkiller.tar.gz)
