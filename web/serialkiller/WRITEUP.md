Writeup
=======

Exploitation d'une désérialisation d'un objet PHP pour faire du *local file read*.

Exploitation&nbsp;:

```bash
pushd exploit/
pip install -r requirements.txt
python exploit.py
popd
```

Flag&nbsp;: `ENSIBS{d0n'7_53r14l1z3_u53r_d474!}`
