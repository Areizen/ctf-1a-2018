<?php

	if(isset($_GET['src'])){
		show_source(__FILE__); // On affiche le code source du fichier
		exit;
	}

	function bonjour($a){
		echo("Bonjour $a !<br/>");
	}
	
	class ENSIBS{
		
		public $fn = "bonjour";
		public $dst = "Charles";
		
		public function bonjour() {
			
			$f = $this->fn;
			$d = $this->dst;
			
			echo("Fonction bonjour: ".$f."<br/>");
			echo("Personne: ".$d."<br/>");
			
			$f($d); // On dit bonjour à la personne
		}
	}

	if(empty($_GET['ENSIBS'])){ // Si get ENSIBS est vide
		
		$obj = new ENSIBS(); // On créé un objet ENSIBS
		$obj->bonjour(); // On appelle bonjour
		
		echo(serialize($obj)."<br/>"); // On affiche l'objet serializé
		
	}else{ // GET plein
	
		$obj = unserialize($_GET['ENSIBS']); // On charge l'objet serializé
		$obj->bonjour(); // On appelle bonjour
		
	}
	
?>