<!doctype html>
<html lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta charset="UTF-8">
        <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet"> 
        <link rel="stylesheet" href="files/style.css"/>
        <title>ZooCheck</title>
    </head>
<body>
    <header>
        <nav>
            <a href="#">Accueil</a>
            <a href="photos.php">Photos</a>
            <a href="shop.php">Shop</a>
            <a href="geo.php">Nous Trouver</a>
            <a href="animaux.php">Animaux</a>
        </nav>
    </header>
    <h1 id="titre">Bienvenue sur ZooCheck !</h1>
    <div id="ptitre">ZooCheck est l'un des zoo les plus visités de Vannes ! Vous y trouverez
    des Gorilles, des Pandas, des Girafes...<br/>
    Pour des raisons de santé, certains animaux ne sont pas exposés certains jours de la semaine. C'est pourquoi nous vous invitons à vérifier l'état des animaux disponibles sur le site web.<br/>
    ZooCheck vous propose également un Shop avec différents produits que vous pourez acheter une fois sur place.<br/>
    Vous pourrez également avoir un apercu du Zoo dans la section photo.<br/>
    Enfin, un section "Nous Trouver" est disponible pour vous aider à rejoindre le Zoo.<br/>
    Bonne visite !<br/><br/>
    <div id="imgtitre"><img src="files/panda-roux.jpg"/></div>
    </div>
</body>
</html>
<?php

?>