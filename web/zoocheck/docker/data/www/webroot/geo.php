<!doctype html>
<html lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta charset="UTF-8">
        <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet"> 
        <link rel="stylesheet" href="files/style.css"/>
        <title>ZooCheck - Photos</title>
    </head>
<body>
    <header>
        <nav>
            <a href="index.php">Accueil</a>
            <a href="photos.php">Photos</a>
            <a href="shop.php">Shop</a>
            <a href="#">Nous Trouver</a>
            <a href="animaux.php">Animaux</a>
        </nav>
    </header>
    <h1 id="titre">Nous trouver</h1>
    <div id="ptitre">
    <h2>Plan GoogleMap</h2>
    <center><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2688.0488810743204!2d-2.750750334517126!3d47.64461694334439!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48101c2db33ef7bf%3A0x9f227f62537a717a!2sE.N.S.I.B.S.%2C+56000+Vannes!5e0!3m2!1sfr!2sfr!4v1521370705366" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></center>
    <h2>Plan du Zoo</h2>
    <div id="imgtitre">
    <img src="files/plan.jpg"/><br/>
    </div>
    </div>
</body>
</html>
<?php

?>