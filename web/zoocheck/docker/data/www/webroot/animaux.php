<?php
    require_once("fonctions.php");
?>
<!doctype html>
<html lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="files/style.css"/>
        <title>ZooCheck - Animaux</title>
    </head>
<body>
    <header>
        <nav>
            <a href="index.php">Accueil</a>
            <a href="photos.php">Photos</a>
            <a href="shop.php">Shop</a>
            <a href="geo.php">Nous Trouver</a>
            <a href="#">Animaux</a>
        </nav>
    </header>
    <h1 id="titre">Animaux</h1>
    <div id="ptitre">Vous pouvez chercher ici les animaux disponibles aux public durant la journée</div><br/><br/>
    <center>
     <?php
            error_reporting(0);
            $req = mysqli_query($link,"SELECT animal FROM animaux");
            $animaux = [];
            while(($row =  mysqli_fetch_assoc($req))) {
                $animaux[] = $row['animal'];
            }
            $i = 1;
            if(isset($_GET['d']) and is_string($_GET['d'])){
                $q = mysqli_query($link,"SELECT * FROM animaux WHERE etat='".($_GET['d'])."'");
            }else{
                $q = mysqli_query($link,"SELECT * FROM animaux");
            }
            if (mysqli_errno($link)) {
                echo("Error:".mysqli_error($link));
            }
    ?>
    <table style="width:600px;">
        <tr>
            <th>Animal</th>
            <th>Etat</th>
        </tr>
        
       <?php
       
            while ($row = mysqli_fetch_assoc($q)){ 
                if(!(in_array(@$row['animal'],$animaux))){
                    $row['animal'] = "?";
                }
                if(@$row['etat'] === '1'){
                    $row['etat'] = "<font style='color:green'>Disponible</font>";
                }elseif(@$row['etat'] === '0'){
                    $row['etat'] = "<font style='color:red'>Indisponible</font>";
                }else{
                    $row['etat'] = "?";
                }
        ?>
        <tr>
            <td><b><?= @$row['animal'] ?></b></td>
            <td><b><?= @$row['etat'] ?></b></td>
        </tr>
        <?php
            $i++;
            }
        ?>
    </table>
    </center>
    <br/><br/>
    <div id="ptitre">
    <h2>Liste des animaux:</h2>
    <center>
    <table id="rechercher"><tr><th><a href="?d=1"><input type="submit" value="Disponibles"></a></th>
    <th><a href="?d=0"><input type="submit" value="Indisponibles"></a></th></tr>
    </table>
    </center>
    </div>
</body>
</html>
<?php

?>
