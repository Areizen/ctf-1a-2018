<?php
require_once("../config.php"); 
?>
<!doctype html>
<html lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../files/style.css"/>
        <title>ZooCheck - Administration</title>
    </head>
<body>
    <header>
        <nav>
            <a href="../index.php">Accueil</a>
            <a href="../photos.php">Photos</a>
            <a href="../shop.php">Shop</a>
            <a href="../geo.php">Nous Trouver</a>
            <a href="../animaux.php">Animaux</a>
        </nav>
    </header>
    <h1 id="titre">Bienvenue sur le panel d'administration !</h1>
    <br/>
    <center>
    <?php
        if(@$_POST['nom'] === $admin_user && @$_POST['pass'] === $admin_pass){ ?>
        <div id="ptitre">
        <h2>Bravo ! le flag est:</h2> <div style="font-family: Helvetica"><?= $_flag; ?></div>
        </div>
    <?php }else{
    ?>
    <form action="" method="POST">
    <table id="admin">
        <tr>
            <th>Nom de compte:</th>
            <th><input type="text" name="nom"></th>
        </tr>
        <tr>
            <th>Mot de passe:</th>
            <th><input type="password" name="pass"></th>
        </tr>
        <tr>
            <th colspan="2"><input type="submit" value="Connexion"></th>
        </tr>
    </table>
    </form>
    </center>
    <?php } ?>
</body>
</html>
<?php

?>
