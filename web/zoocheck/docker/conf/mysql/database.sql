SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';


-- ----------------------------
-- Table structure for administration
-- ----------------------------
DROP TABLE IF EXISTS `administration`;
CREATE TABLE `administration` (
  `login` varchar(255) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of administration
-- ----------------------------
INSERT INTO `administration` VALUES ('4dminIsTrat0r_logiN', 'VhC8T+LFtc2&&4nE]:V@4Cm{');

-- ----------------------------
-- Table structure for animaux
-- ----------------------------
DROP TABLE IF EXISTS `animaux`;
CREATE TABLE `animaux` (
  `animal` varchar(255) DEFAULT NULL,
  `etat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of animaux
-- ----------------------------
INSERT INTO `animaux` VALUES ('gorille', '1');
INSERT INTO `animaux` VALUES ('girafe', '1');
INSERT INTO `animaux` VALUES ('panda', '1');
INSERT INTO `animaux` VALUES ('panda-roux', '0');
INSERT INTO `animaux` VALUES ('zebre', '1');
INSERT INTO `animaux` VALUES ('rhinocéros', '0');
INSERT INTO `animaux` VALUES ('élephant', '1');
INSERT INTO `animaux` VALUES ('flamant', '1');
