SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

USE db;

-- ----------------------------
-- Table structure for jeux
-- ----------------------------
DROP TABLE IF EXISTS `jeux`;
CREATE TABLE `jeux` (
  `nom` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `prix` varchar(255) DEFAULT NULL,
  `support` varchar(255) DEFAULT NULL,
  `editeur` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jeux
-- ----------------------------
INSERT INTO `jeux` VALUES ('Mario', 'https://vignette.wikia.nocookie.net/fantendo/images/0/0e/NES_Mario.jpg', '26.99$', 'NES', 'Nintendo');
INSERT INTO `jeux` VALUES ('Donkey Kong', 'http://www.nesfiles.com/NES/Donkey_Kong_Classics/Donkey_Kong_Classics_cart.jpg', '26.95$', 'NES', 'Nintendo');
INSERT INTO `jeux` VALUES ('Tortues Ninja IV', 'https://i2.cdscdn.com/pdt2/3/7/4/1/700x700/3307210248374/rw/les-tortues-ninja-gba.jpg', '40.11$', 'GameBoy Advance', 'Ubisoft');
INSERT INTO `jeux` VALUES ('Street Fighter II', 'https://upload.wikimedia.org/wikipedia/fr/5/56/Street_Fighter_II_The_World_Warrior_logo.png', '37.61$', 'Commodore 64', 'Capcom');
INSERT INTO `jeux` VALUES ('Doom', 'https://vignette.wikia.nocookie.net/doom/images/8/85/Doom.jpg', '14.07$', 'DOS', 'id Software');
INSERT INTO `jeux` VALUES ('SSX 3', 'http://image.jeuxvideo.com/images/p2/s/s/ssx3p20f.jpg', '12.05$', 'PlayStation 2', 'EA Sports');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user` varchar(255) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('admin', 'S€cr3t-p4$$w0o0rd!');
