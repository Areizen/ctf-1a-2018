<?php
require_once("config.php"); 
?>
<!doctype html>
<html lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta charset="UTF-8">
		<link rel="shortcut icon" type="image/png" href="files/ico.png" />
		<link rel="icon" type="image/png" href="files/ico.png" />
		<meta name="theme-color" content="#9842F4">
        <link rel="stylesheet" href="files/style.css"/>
        <title>MyGame - Administration</title>
    </head>
<body>
    <header>
        <nav>
            <a href="index.php">Accueil</a>
            <a href="rechercher.php">Rechercher</a>
            <a href="apropos.php">A propos</a>
        </nav>
    </header>
    <h1>Bienvenue sur le panel d'administration !</h1>
    <br/>
    <center>
    <?php
        if(@$_POST['nom'] === $admin_user && @$_POST['pass'] === $admin_pass){ ?>
        
        <h1>Bravo ! le flag est: <div style="font-family: Helvetica"><?= $_flag; ?></div></h1>
        
    <?php }else{
    ?>
    <form action="" method="POST">
    <table id="admin">
        <tr>
            <th>Nom de compte:</th>
            <th><input type="text" name="nom"></th>
        </tr>
        <tr>
            <th>Mot de passe:</th>
            <th><input type="password" name="pass"></th>
        </tr>
        <tr>
            <th colspan="2"><input type="submit" value="Connexion"></th>
        </tr>
    </table>
    </form>
    </center>
    <?php } ?>
    <img src="files/mario.png"/>
</body>
</html>
<?php

?>
