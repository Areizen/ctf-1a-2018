<?php
    require_once("fonctions.php");
?>
<!doctype html>
<html lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta charset="UTF-8">
		<link rel="shortcut icon" type="image/png" href="files/ico.png" />
		<link rel="icon" type="image/png" href="files/ico.png" />
		<meta name="theme-color" content="#9842F4">
        <link rel="stylesheet" href="files/style.css"/>
        <title>MyGame - Rechercher</title>
    </head>
<body>
    <header>
        <nav>
            <a href="index.php">Accueil</a>
            <a href="#">Rechercher</a>
            <a href="apropos.php">A propos</a>
            <!--<a href="admin.php">Administration</a>-->
        </nav>
    </header>
    <h1>Liste des jeux</h1>
    <center>
     <?php
            $i = 1;
            if(isset($_POST['nom']) and !empty($_POST['nom']) and is_string($_POST['nom'])){
                $q = mysqli_query($link,"SELECT * FROM jeux WHERE nom LIKE '%".($_POST['nom'])."%'");
            }else{
                $q = mysqli_query($link,"SELECT * FROM jeux");
            }
            if (mysqli_errno($link)) {
                echo("Error:".mysqli_error($link));
            }
    ?>
    <table>
        <tr>
            <th>N°</th>
            <th>Nom</th>
            <th>Image</th>
            <th>Prix</th>
            <th>Support</th>
            <th>Editeur</th>
        </tr>
        
       <?php
       
        error_reporting(0);
            while ($row = mysqli_fetch_assoc($q)){ 
        ?>
        <tr>
            <td><?= @$i ?></td>
            <td><?= @$row['nom'] ?></td>
            <td><img src="<?= @$row['image'] ?>"/></td>
            <td><?= @$row['prix'] ?></td>
            <td><?= @$row['support'] ?></td>
            <td><?= @$row['editeur'] ?></td>
        </tr>
        <?php
            $i++;
            }
        ?>
    </table>
    </center>
    <h1 style="font-size:25pt;">Rechercher un jeu:</h1>
    <center>
    <form action="" method="POST">
    <table id="rechercher">
        <tr>
            <th>Nom:</th>
            <th><input type="text" name="nom" placeholder="Mario"></th>
            <th><input type="submit" value="Chercher"></th>
        </tr>
    </table>
    </form>
    </center>
</body>
</html>
<?php

?>
