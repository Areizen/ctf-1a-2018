#
# Nginx Dockerfile
#
# Written by:
#   Baptiste MOINE <contact@bmoine.fr>
#

# Pull base image.
FROM debian:jessie

MAINTAINER Baptiste MOINE <contact@bmoine.fr>

ENV OPENSSL_VERSION=1.1.0g \
    NGINX_VERSION=1.13.8

# Install minimal tools.
RUN apt-get update && apt-get -y install \
    curl \
    wget \
    git \
    subversion \
    mercurial \
    ntpdate \
    bzip2 \
    unzip

# Install compilation tools.
RUN apt-get -y install \
    autoconf \
    automake \
    build-essential \
    ca-certificates \
    g++ \
    gcc \
    g++-multilib \
    gcc-multilib \
    ca-certificates \
    make \
    perl \
    python3 \
    libaprutil1 \
    libaprutil1-dev \
    libatomic-ops-dev \
    libgd-dev \
    libgeoip-dev \
    libpcre3-dev \
    libperl-dev \
    libreadline-dev \
    libxml2 \
    libxml2-dev \
    libxml2-utils \
    libyaml-dev \
    zlib1g-dev

# Install nginx.
RUN cd /tmp/ \
    && LAST_HEADER_MORE_NGINX_MODULE=$(curl --silent -X GET --url https://api.github.com/repos/openresty/headers-more-nginx-module/tags | python3 -c "import sys, json, re; from distutils.version import StrictVersion; raw = json.load(sys.stdin); test  = sorted(raw, key=lambda k: re.sub(r'^v?((?:(?:[0-9]+\.){1,}(?:[0-9]+){1,}|(?:[0-9]+))).*', r'\1', k['name']), reverse=True); print(test[0]['name'])") \
    && export LAST_HEADER_MORE_NGINX_MODULE=$(python3 -c "import re; print(re.sub(r'^v?((?:(?:[0-9]+\.){1,}(?:[0-9]+){1,}|(?:[0-9]+))).*', r'\1', '${LAST_HEADER_MORE_NGINX_MODULE}'))") \
    && wget https://github.com/openresty/headers-more-nginx-module/archive/v${LAST_HEADER_MORE_NGINX_MODULE}.tar.gz \
    && wget https://www.openssl.org/source/openssl-${OPENSSL_VERSION}.tar.gz \
    && wget http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz \
    && tar xvzf openssl-${OPENSSL_VERSION}.tar.gz \
    && tar xvzf nginx-${NGINX_VERSION}.tar.gz \
    && tar xvzf v${LAST_HEADER_MORE_NGINX_MODULE}.tar.gz \
    && cd nginx-${NGINX_VERSION}/ \
    && ./configure \
        --prefix=/etc/nginx \
        --sbin-path=/usr/sbin/nginx \
        --modules-path=/usr/lib/nginx/modules \
        --conf-path=/etc/nginx/nginx.conf \
        --error-log-path=/var/log/nginx/error.log \
        --http-log-path=/var/log/nginx/access.log \
        --pid-path=/var/run/nginx.pid \
        --lock-path=/var/run/nginx.lock \
        --http-client-body-temp-path=/var/cache/nginx/client_temp \
        --http-proxy-temp-path=/var/cache/nginx/proxy_temp \
        --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp \
        --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp \
        --http-scgi-temp-path=/var/cache/nginx/scgi_temp \
        --user=nginx \
        --group=nginx \
        --add-dynamic-module=../headers-more-nginx-module-${LAST_HEADER_MORE_NGINX_MODULE} \
        --with-compat \
        --with-file-aio \
        --with-ipv6 \
        --with-threads \
        --with-http_addition_module \
        --with-http_auth_request_module \
        --with-http_dav_module \
        --with-http_flv_module \
        --with-http_geoip_module \
        --with-http_gunzip_module \
        --with-http_gzip_static_module \
        --with-http_image_filter_module \
        --with-http_mp4_module \
        --with-http_random_index_module \
        --with-http_realip_module \
        --with-http_secure_link_module \
        --with-http_slice_module \
        --with-http_ssl_module \
        --with-http_stub_status_module \
        --with-http_sub_module \
        --with-http_v2_module \
        --with-libatomic \
        --with-mail \
        --with-mail_ssl_module \
        --with-stream \
        --with-stream_geoip_module \
        --with-stream_realip_module \
        --with-stream_ssl_module \
        --with-stream_ssl_preread_module \
        --with-openssl=../openssl-${OPENSSL_VERSION} \
        --with-pcre \
        --with-pcre-jit \
        --with-cc-opt='-m64 -g -O3 -fuse-ld=gold -fstack-protector-strong --param=ssp-buffer-size=4 -Wformat -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -fPIC' \
        --with-ld-opt='-Wl,-z,relro -Wl,-z,now -Wl,--as-needed -pie' \
    && make \
    && make install \
    && useradd -M -s /bin/false nginx \
    && mkdir /var/cache/nginx

# Create nginx file structure.
RUN rm -rf /usr/share/nginx/html/ \
    && mkdir -p /usr/share/nginx/static \
    && mkdir -p /usr/share/nginx/webroot \
    && mkdir -p /etc/ssl/private/ \
    && mkdir -p /tmp/.acme-challenge/

# Install Let's Encrypt.
RUN apt-get install -y python3-pip \
    && cd /opt/ \
    && git clone https://github.com/certbot/certbot \
    && cd certbot/ \
    && ./certbot-auto --os-packages-only --non-interactive

# Cleanup.
RUN apt-get clean \
    && rm -rf /tmp/*

# Create volumes.
VOLUME ["/etc/nginx/", "/usr/share/nginx/", "/etc/ssl/private/"]

# Set workdir.
WORKDIR /usr/share/nginx/webroot/

ADD ./start.sh /start.sh
RUN chmod +x /start.sh

# TCP port that container will listen for connections.
# HTTP and HTTPS.
EXPOSE 80/tcp 443/tcp

CMD ["/start.sh"]

