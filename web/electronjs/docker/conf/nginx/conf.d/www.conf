server {
    listen                                          80;
    listen                                          [::]:80;
    server_name                                     electronjs.ensibs.ctf;

    more_set_headers                                "Server: ElectronJS by Areizen";

    root                                            /usr/share/nginx/webroot/;
    index                                           index.php index.html index.htm;

    access_log                                      /usr/share/nginx/log/www.access.log main;
    error_log                                       /usr/share/nginx/log/www.error.log warn;

    charset                                         utf-8;
    keepalive_timeout                               70;

    location / {
        include                                     www_common.conf;

        try_files                                   $uri $uri/ =404;

        error_page                                  404 /404.html;
        error_page                                  500 502 503 504 /50x.html;

        location ~ \.php$ {
            try_files                               $uri =404;
            fastcgi_index                           index.php;
            fastcgi_intercept_errors                on;
            fastcgi_param                           HTTP_PROXY "";
            fastcgi_pass                            phpfcgi;
            fastcgi_split_path_info                 ^(.+\.php)(/.+)$;
            fastcgi_param SCRIPT_FILENAME           $document_root$fastcgi_script_name;
            include                                 /etc/nginx/fastcgi_params;
        }

        location ~* \.(ico|css|js|gif|jpe?g|png)$ {
            expires                                 max;
            log_not_found                           off;
            access_log                              off;
            add_header                              Pragma public;
            add_header                              Cache-Control "public, must-revalidate, proxy-revalidate";
        }

        location ~ /\.ht {
            deny                                    all;
        }

        error_page                                  404 /404.html;
        error_page                                  500 502 503 504 /50x.html;

        location = /404.html {
            root                                    /usr/share/nginx/static/;
        }

        location = /50x.html {
            root                                    /usr/share/nginx/static/;
        }
    }
}
