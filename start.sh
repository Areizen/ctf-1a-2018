#!/bin/bash

echo "Starting Pwn containers"
pushd pwn/crypto-buffer/docker; ./start.sh; popd
pushd pwn/pass-leak/docker; ./start.sh; popd
pushd pwn/rop/docker; ./start.sh; popd

echo "Starting Misc containers"
pushd misc/welcome/docker; ./start.sh; popd
pushd misc/not-so-white/docker; ./start.sh; popd

echo "Starting Web containers"
pushd web/electronjs/docker; ./start.sh; popd
pushd web/harpoon/docker; ./start.sh; popd
pushd web/hash/docker; ./start.sh; popd
pushd web/jwatson/docker; ./start.sh; popd
pushd web/myobject/docker; ./start.sh; popd
pushd web/serialkiller/docker; ./start.sh; popd
pushd web/mygame/docker; ./start.sh; popd
pushd web/zoocheck/docker; ./start.sh; popd

echo "Starting Crypto containers"
pushd crypto/cbc/docker; ./start.sh; popd
pushd crypto/dancingmen/docker; ./start.sh; popd

echo "All containers should be up (please check!)"
screen -ls

